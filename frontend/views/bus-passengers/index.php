<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel frontend\search\BusPassengersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bus Passengers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-passengers-index">
    <p>        
        <?= Html::button('Add Mitra', ['value' => Url::to('/intra/bus-passengers/create'), 'class' => 'btn btn-success', 'id' => 'modalButton']) ?>
    </p>

    <?php
    Modal::begin([
        'header' => '<h4>Add Passenger</h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);

    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>    
    
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'busPassengerId',
            'tripScheduleId',
            'busPassengerName',
            'busPassengerAddress',
            'busPassengerPhone',
            //'unitPrice',
            //'totalSeats',
            //'totalPrice',
            //'totalPaid',
            //'restPayment',
            //'isMultiSeat',
            //'bookingCode',
            //'seatNumber',
            //'isLuggage',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>


</div>   