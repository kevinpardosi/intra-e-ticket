<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\search\BusPassengersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bus-passengers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'busPassengerId') ?>

    <?= $form->field($model, 'tripScheduleId') ?>

    <?= $form->field($model, 'busPassengerName') ?>

    <?= $form->field($model, 'busPassengerAddress') ?>

    <?= $form->field($model, 'busPassengerPhone') ?>

    <?php // echo $form->field($model, 'unitPrice') ?>

    <?php // echo $form->field($model, 'totalSeats') ?>

    <?php // echo $form->field($model, 'totalPrice') ?>

    <?php // echo $form->field($model, 'totalPaid') ?>

    <?php // echo $form->field($model, 'restPayment') ?>

    <?php // echo $form->field($model, 'isMultiSeat') ?>

    <?php // echo $form->field($model, 'bookingCode') ?>

    <?php // echo $form->field($model, 'seatNumber') ?>

    <?php // echo $form->field($model, 'isLuggage') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
