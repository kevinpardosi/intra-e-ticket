<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\BusPassengers */

$this->title = $model->busPassengerId;
$this->params['breadcrumbs'][] = ['label' => 'Bus Passengers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bus-passengers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->busPassengerId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->busPassengerId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'busPassengerId',
            'tripScheduleId',
            'busPassengerName',
            'busPassengerAddress',
            'busPassengerPhone',
            'unitPrice',
            'totalSeats',
            'totalPrice',
            'totalPaid',
            'restPayment',
            'isMultiSeat',
            'bookingCode',
            'seatNumber',
            'isLuggage',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
        ],
    ]) ?>

</div>
