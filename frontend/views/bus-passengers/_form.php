<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\BusPassengers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bus-passengers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tripScheduleId')->textInput() ?>

    <?= $form->field($model, 'busPassengerName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'busPassengerAddress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'busPassengerPhone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unitPrice')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'totalSeats')->textInput() ?>

    <?= $form->field($model, 'totalPrice')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'totalPaid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'restPayment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isMultiSeat')->textInput() ?>

    <?= $form->field($model, 'bookingCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seatNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isLuggage')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
