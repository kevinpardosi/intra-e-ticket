<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "bus_passengers".
 *
 * @property int $busPassengerId
 * @property int $tripScheduleId
 * @property string $busPassengerName
 * @property string $busPassengerAddress
 * @property string $busPassengerPhone
 * @property string $unitPrice
 * @property int $totalSeats
 * @property string $totalPrice
 * @property string $totalPaid
 * @property string $restPayment
 * @property int $isMultiSeat
 * @property string $bookingCode
 * @property string $seatNumber
 * @property int $isLuggage
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class BusPassengers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bus_passengers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tripScheduleId', 'busPassengerName', 'unitPrice', 'totalSeats', 'totalPrice', 'totalPaid', 'restPayment', 'isMultiSeat', 'seatNumber'], 'required'],
            [['tripScheduleId', 'totalSeats', 'isMultiSeat', 'isLuggage', 'created_by', 'updated_by'], 'integer'],
            [['unitPrice', 'totalPrice', 'totalPaid', 'restPayment'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['busPassengerName'], 'string', 'max' => 64],
            [['busPassengerAddress', 'seatNumber'], 'string', 'max' => 128],
            [['busPassengerPhone'], 'string', 'max' => 16],
            [['bookingCode'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'busPassengerId' => 'Bus Passenger ID',
            'tripScheduleId' => 'Trip Schedule ID',
            'busPassengerName' => 'Bus Passenger Name',
            'busPassengerAddress' => 'Bus Passenger Address',
            'busPassengerPhone' => 'Bus Passenger Phone',
            'unitPrice' => 'Unit Price',
            'totalSeats' => 'Total Seats',
            'totalPrice' => 'Total Price',
            'totalPaid' => 'Total Paid',
            'restPayment' => 'Rest Payment',
            'isMultiSeat' => 'Is Multi Seat',
            'bookingCode' => 'Booking Code',
            'seatNumber' => 'Seat Number',
            'isLuggage' => 'Is Luggage',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
