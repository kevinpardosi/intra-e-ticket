//                'template' => '{sell} {print} {delete}',
//                'buttons' => [
//                    'sell' => function ($url, $model) {
//                        return Html::a('<button type="button" class="btn btn-success"><i class="glyphicon glyphicon-shopping-cart"></i></button>', $url, [
//                                    'title' => Yii::t('app', 'Sell Tickets'),
//                                    'data-toggle' => "modal",
//                                    'data-target' => "#myModal",
//                                    'data-method' => 'post',
//                        ]);
//                    },
//                    'print' => function ($url, $model) {
//                        return Html::a('<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-print"></i></button>', $url, [
//                                    'title' => Yii::t('app', 'Print Trip Permit'),
//                                    'data-toggle' => "modal",
//                                    'data-target' => "#myModal",
//                                    'data-method' => 'post',
//                        ]);
//                    },
//                    'delete' => function ($url, $model) {
//                        return Html::a('<button type="button" class="btn btn-danger"><i class="glyphicon glyphicon-remove-sign"></i></button>', $url, [
//                                    'title' => Yii::t('app', 'Reject'),
//                                    'data-toggle' => "modal",
//                                    'data-target' => "#myModal",
//                                    'data-method' => 'post',
//                        ]);
//                    },
//                ],
//                'urlCreator' => function ($action, $model, $key, $index) {
//                    if ($action == 'sell') {
//                        $url = Url::toRoute(['trip/sell', 'id' => $model->tripScheduleId]);
//                        return $url;
//                    } else if ($action == ''){
//                        $url = Url::toRoute(['trip/print', 'id' => $model->tripScheduleId]);
//                        return $url;
//                    } else {
//                        $url = Url::toRoute(['trip/remove', 'id' => $model->tripScheduleId]);
//                        return $url;
//                    }
//                },