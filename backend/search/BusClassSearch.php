<?php

namespace backend\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BusClass;

/**
 * BusClassSearch represents the model behind the search form of `app\models\BusClass`.
 */
class BusClassSearch extends BusClass
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['busClassId', 'created_by', 'updated_by'], 'integer'],
            [['busClassCode', 'busClassName', 'busClassNote', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BusClass::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'busClassId' => $this->busClassId,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'busClassCode', $this->busClassCode])
            ->andFilterWhere(['like', 'busClassName', $this->busClassName])
            ->andFilterWhere(['like', 'busClassNote', $this->busClassNote]);

        return $dataProvider;
    }
}
