<?php

namespace backend\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BusDrivers;

/**
 * BusDriversSearch represents the model behind the search form of `app\models\BusDrivers`.
 */
class BusDriversSearch extends BusDrivers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['busDriverId', 'driverStatus', 'is_expired'], 'integer'],
            [['busDriverCode', 'busDriverName', 'busDriverPhone', 'busDriverAddress', 'licenseExpiredAt', 'idCard', 'drivingLicense', 'busDriverNote'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BusDrivers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'busDriverId' => $this->busDriverId,
            'driverStatus' => $this->driverStatus,
            'licenseExpiredAt' => $this->licenseExpiredAt,
            'is_expired' => $this->is_expired,
        ]);

        $query->andFilterWhere(['like', 'busDriverCode', $this->busDriverCode])
            ->andFilterWhere(['like', 'busDriverName', $this->busDriverName])
            ->andFilterWhere(['like', 'busDriverPhone', $this->busDriverPhone])
            ->andFilterWhere(['like', 'busDriverAddress', $this->busDriverAddress])
            ->andFilterWhere(['like', 'busDriverNote', $this->busDriverNote])
            ->andFilterWhere(['like', 'idCard', $this->idCard])
            ->andFilterWhere(['like', 'drivingLicense', $this->drivingLicense]);

        return $dataProvider;
    }
}
