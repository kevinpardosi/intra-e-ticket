<?php

namespace backend\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BusRoutes;

/**
 * BusRoutesSearch represents the model behind the search form of `app\models\BusRoutes`.
 */
class BusRoutesSearch extends BusRoutes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['busRouteId', 'busClassId', 'busDepartureId', 'busDestinationId', 'busRouteCount', 'created_by', 'updated_by'], 'integer'],
            [['ticketPrice'], 'number'],
            [['busRouteNote', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BusRoutes::find();
        
//        $query->joinWith(['areaname']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'busRouteId' => $this->busRouteId,
            'busClassId' => $this->busClassId,
            'busDepartureId' => $this->busDepartureId,
            'busDestinationId' => $this->busDestinationId,
            'ticketPrice' => $this->ticketPrice,
            'busRouteCount' => $this->busRouteCount,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'busRouteNote', $this->busRouteNote]);

        return $dataProvider;
    }
}
