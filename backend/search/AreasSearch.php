<?php

namespace backend\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Areas;

/**
 * AreasSearch represents the model behind the search form of `backend\models\Areas`.
 */
class AreasSearch extends Areas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['areaId', 'areaDeptCount', 'areaDestCount'], 'integer'],
            [['areaName', 'areaNote'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Areas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'areaId' => $this->areaId,
            'areaDeptCount' => $this->areaDeptCount,
            'areaDestCount' => $this->areaDestCount,
        ]);

        $query->andFilterWhere(['like', 'areaName', $this->areaName])
            ->andFilterWhere(['like', 'areaNote', $this->areaNote]);

        return $dataProvider;
    }
}
