<?php

namespace backend\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TripsSchedule;

/**
 * TripsScheduleSearch represents the model behind the search form of `backend\models\TripsSchedule`.
 */
class TripsScheduleSearch extends TripsSchedule
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['busDriverId', 'busCoDriverId', 'tripScheduleId', 'busFleetId', 'busRouteId', 'tripStatus', 'isBooked', 'created_by', 'updated_by'], 'integer'],
            [['tripNumber', 'departureTime', 'departureDate', 'tripScheduleNote', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TripsSchedule::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tripScheduleId' => $this->tripScheduleId,
            'busFleetId' => $this->busFleetId,
            'busRouteId' => $this->busRouteId,
            'busDriverId' => $this->busDriverId,
            'busCoDriverId' => $this->busCoDriverId,
            'departureTime' => $this->departureTime,
            'departureDate' => $this->departureDate,
            'tripStatus' => $this->tripStatus,
            'isBooked' => $this->isBooked,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'tripNumber', $this->tripNumber])
            ->andFilterWhere(['like', 'tripScheduleNote', $this->tripScheduleNote]);

        return $dataProvider;
    }
}
