<?php

namespace backend\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BusFleet;

/**
 * BusFleetSearch represents the model behind the search form of `app\models\BusFleet`.
 */
class BusFleetSearch extends BusFleet
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['busFleetId', 'busClassId', 'busSeatsCount', 'busFleetStatus'], 'integer'],
            [['busLicensePlate', 'busFleetNote'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BusFleet::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'busFleetId' => $this->busFleetId,
            'busClassId' => $this->busClassId,
            'busSeatsCount' => $this->busSeatsCount,
            'busFleetStatus' => $this->busFleetStatus,
        ]);

        $query->andFilterWhere(['like', 'busLicensePlate', $this->busLicensePlate])
            ->andFilterWhere(['like', 'busFleetNote', $this->busFleetNote]);

        return $dataProvider;
    }
}
