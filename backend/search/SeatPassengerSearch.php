<?php

namespace backend\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\SeatPassenger;

/**
 * SeatPassengerSearch represents the model behind the search form of `backend\models\SeatPassenger`.
 */
class SeatPassengerSearch extends SeatPassenger
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['seatPassengerId', 'tripScheduleId', 'seat-1', 'seat-2', 'seat-3', 'seat-4', 'seat-5', 'seat-6', 'seat-7', 'seat-8', 'seat-9', 'seat-10', 'seat-11', 'seat-12', 'seat-13', 'seat-14', 'seat-15', 'seat-16', 'seat-17', 'seat-18', 'seat-19', 'seat-20', 'seat-21', 'seat-22', 'seat-23', 'seat-24', 'seat-25', 'seat-26', 'seat-27', 'seat-28', 'seat-29', 'seat-30', 'seat-31', 'seat-32', 'seat-33', 'seat-34', 'seat-35', 'seat-36', 'seat-37', 'seat-38', 'seat-39', 'seat-40', 'seat-41', 'seat-42', 'isOcuppied', 'maintenanceSeat-1', 'maintenanceSeat-2', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SeatPassenger::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'seatPassengerId' => $this->seatPassengerId,
            'tripScheduleId' => $this->tripScheduleId,
            'seat-1' => $this->seat-1,
            'seat-2' => $this->seat-2,
            'seat-3' => $this->seat-3,
            'seat-4' => $this->seat-4,
            'seat-5' => $this->seat-5,
            'seat-6' => $this->seat-6,
            'seat-7' => $this->seat-7,
            'seat-8' => $this->seat-8,
            'seat-9' => $this->seat-9,
            'seat-10' => $this->seat-10,
            'seat-11' => $this->seat-11,
            'seat-12' => $this->seat-12,
            'seat-13' => $this->seat-13,
            'seat-14' => $this->seat-14,
            'seat-15' => $this->seat-15,
            'seat-16' => $this->seat-16,
            'seat-17' => $this->seat-17,
            'seat-18' => $this->seat-18,
            'seat-19' => $this->seat-19,
            'seat-20' => $this->seat-20,
            'seat-21' => $this->seat-21,
            'seat-22' => $this->seat-22,
            'seat-23' => $this->seat-23,
            'seat-24' => $this->seat-24,
            'seat-25' => $this->seat-25,
            'seat-26' => $this->seat-26,
            'seat-27' => $this->seat-27,
            'seat-28' => $this->seat-28,
            'seat-29' => $this->seat-29,
            'seat-30' => $this->seat-30,
            'seat-31' => $this->seat-31,
            'seat-32' => $this->seat-32,
            'seat-33' => $this->seat-33,
            'seat-34' => $this->seat-34,
            'seat-35' => $this->seat-35,
            'seat-36' => $this->seat-36,
            'seat-37' => $this->seat-37,
            'seat-38' => $this->seat-38,
            'seat-39' => $this->seat-39,
            'seat-40' => $this->seat-40,
            'seat-41' => $this->seat-41,
            'seat-42' => $this->seat-42,
            'isOcuppied' => $this->isOcuppied,
            'maintenanceSeat-1' => $this->maintenanceSeat-1,
            'maintenanceSeat-2' => $this->maintenanceSeat-2,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
