<?php

namespace backend\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BusPassengers;

/**
 * BusPassengersSearch represents the model behind the search form of `app\models\BusPassengers`.
 */
class BusPassengersSearch extends BusPassengers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['busPassengerId', 'tripScheduleId', 'totalSeats', 'isMultiSeat', 'isLuggage', 'created_by', 'updated_by'], 'integer'],
            [['busPassengerName', 'busPassengerAddress', 'busPassengerPhone', 'bookingCode', 'seatNumber', 'created_at', 'updated_at'], 'safe'],
            [['unitPrice', 'totalPrice', 'totalPaid', 'restPayment', 'totalChange'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BusPassengers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'busPassengerId' => $this->busPassengerId,
            'tripScheduleId' => $this->tripScheduleId,
            'unitPrice' => $this->unitPrice,
            'totalSeats' => $this->totalSeats,
            'totalPrice' => $this->totalPrice,
            'totalPaid' => $this->totalPaid,
            'totalChange' => $this->totalChange,
            'restPayment' => $this->restPayment,
            'isMultiSeat' => $this->isMultiSeat,
            'isLuggage' => $this->isLuggage,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'busPassengerName', $this->busPassengerName])
            ->andFilterWhere(['like', 'busPassengerAddress', $this->busPassengerAddress])
            ->andFilterWhere(['like', 'busPassengerPhone', $this->busPassengerPhone])
            ->andFilterWhere(['like', 'bookingCode', $this->bookingCode])
            ->andFilterWhere(['like', 'seatNumber', $this->seatNumber]);

        return $dataProvider;
    }
}
