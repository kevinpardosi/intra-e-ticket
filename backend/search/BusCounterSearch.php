<?php

namespace backend\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BusCounter;

/**
 * BusCounterSearch represents the model behind the search form of `backend\models\BusCounter`.
 */
class BusCounterSearch extends BusCounter
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['busCounterId', 'created_by', 'updated_by', 'isActive', 'ticketCounter'], 'integer'],
            [['counterCode', 'address', 'phone', 'created_at', 'updated_at', 'counterName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BusCounter::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'busCounterId' => $this->busCounterId,
            'ticketCounter' => $this->ticketCounter,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'isActive' => $this->isActive,
        ]);

        $query->andFilterWhere(['like', 'counterCode', $this->counterCode])
            ->andFilterWhere(['like', 'counterName', $this->counterName])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}
