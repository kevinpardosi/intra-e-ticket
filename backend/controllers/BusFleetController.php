<?php

namespace backend\controllers;

use Yii;
use backend\models\BusFleet;
use backend\models\SystemStatus;
use backend\search\BusFleetSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\json;

/**
 * BusFleetController implements the CRUD actions for BusFleet model.
 */
class BusFleetController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BusFleet models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new BusFleetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BusFleet model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
     public function actionView2($id) {
          $model=$this->findModel($id);
            if (isset($_POST['hasEditable'])) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                      if($model->load($_POST)  ){
                            $param = $_POST;
                    unset($param['_csrf-backend']);
                    unset($param['hasEditable']);
                    $key = array_keys($param);
                    $value = array_values($param);
                  
                    if ($model->save(false)) {
                        $message = '';
                    } else {
                        $message = $model->getErrors($key[0]);
                    }
                    return ['output' => $_POST, 'message' => $message];
                      }
                  
            }
        
          return $this->render('view2', [
                    'model' => $model,
        ]);
     }
    public function actionView($id) {

        $model = new BusFleet();
        if (Yii::$app->request->post('hasEditable')) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $_id = Yii::$app->request->post('editableKey');
            $model = BusFleet::findOne($_id);

            $out = Json::encode(['output' => '', 'message' => '']);


            $post = [];
            $posted = current($_POST['BusFleet']);

            $post['BusFleet'] = $posted;
//            echo "<pre>";
//            print_r($posted);
//            die();

            if ($model->load($_POST)) {
                $attribute = $posted['editableAttribute'];
                if ($attribute == "busFleetStatus") {
                    $modelSystem = SystemStatus::find()->where(['statusCode' => 'busFleetStatus', 'statusDetail' => $posted['name']])->one();
                    $model->busFleetStatus = $modelSystem->systemStatusId;
                }
                $model->save();
                if (isset($posted['editableAttribute'])) {
                    $output = $posted['editableAttribute'];
                }
                 \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $param = $_POST;
                    unset($param['_csrf-frontend']);
                    unset($param['hasEditable']);
                    $key = array_keys($param);
                    $value = array_values($param);
                    if ($model->save()) {
                        $message = '';
                    } else {
                        $message = $model->getErrors($key[0]);
                    }
                      return ['output' =>$value[0] , 'message' => $message];
                
            } 
            return $out;
        } 
        else{
            return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
        }

        
    }

    /**
     * Creates a new BusFleet model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new BusFleet();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->busFleetId]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing BusFleet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->busFleetId]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BusFleet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BusFleet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BusFleet the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = BusFleet::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
