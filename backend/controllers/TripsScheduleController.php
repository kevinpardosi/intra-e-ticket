<?php

namespace backend\controllers;

use Yii;
use backend\models\TripsSchedule;
use backend\search\TripsScheduleSearch;
use backend\models\BusPassengers;
use backend\search\BusPassengersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use nickdenry\grid\toggle\actions\ToggleAction;
use yii\data\ActiveDataProvider;

/**
 * TripsScheduleController implements the CRUD actions for TripsSchedule model.
 */
class TripsScheduleController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TripsSchedule models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new TripsScheduleSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => TripsSchedule::find()->where(['created_by' => Yii::$app->user->id]),
            'pagination' => [
                'pageSize' => 10
            ],
        ]);
        $dataProvider->setSort([
            'defaultOrder' => [
                    'tripScheduleId' => SORT_DESC
                ]
        ]); 

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTicketSales() {
        $base = new BaseController();
        $base->updateTripStatus();

        $searchModel = new TripsScheduleSearch();
//        $searchModel->departureDate = $base->getCurrentDate();
        $dataProvider = new ActiveDataProvider([
            'query' => TripsSchedule::find()->where(['created_by' => Yii::$app->user->id, 'tripStatus' => 10, 'departureDate' => $base->getCurrentDate()]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        $dataProvider->setSort([
            'defaultOrder' => ['departureTime' => SORT_ASC]
        ]); 

        return $this->render('ticket-sales', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TripsSchedule model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Displays a single TripsSchedule model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionTicketSalesInfo($id) {
        $searchModel = new BusPassengersSearch();
        $dataProviderPassenger = $searchModel->search(Yii::$app->request->queryParams);
        $dataProviderPassenger->pagination = [
            'pageSize' => 5
        ];
        
        return $this->render('ticket-sales-info', [
                    'modelTrip' => $this->findModel($id),
                    'dataProviderPassenger' => $dataProviderPassenger,
        ]);
    }

    /**
     * Creates a new TripsSchedule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new TripsSchedule();
        $base = new BaseController();

        if ($model->load(Yii::$app->request->post())) {

            $model->created_at = $base->createdAt();
            $model->created_by = $base->createdBy();
            $model->tripStatus = 10;
            $model->isBooked = 8;

            $model->save();

            return $this->redirect(['view', 'id' => $model->tripScheduleId]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing TripsSchedule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->tripScheduleId]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TripsSchedule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TripsSchedule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TripsSchedule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = TripsSchedule::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionUpdateTripStatus($id) {
        $model = $this->findModel($id);
        $base = new BaseController();

        $currTime = $base->getCurrentTime();

        $status = $model->tripStatus;
        if ($status == 9) {
            if ($model->departureTime < $currTime) {
                $model->tripStatus = 10;
            }                        
        } else if ($status == 10) {
            if ($model->departureTime >= $currTime) {
                $model->tripStatus = 9;
            }            
        }

        $model->save();

        return $this->redirect(['ticket-sales']);
    }   
}
