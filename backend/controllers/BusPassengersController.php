<?php

namespace backend\controllers;

use Yii;
use backend\models\BusPassengers;
use backend\models\BusCounter;
use backend\models\BusClass;
use backend\models\User;
use backend\search\BusPassengersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BusPassengersController implements the CRUD actions for BusPassengers model.
 */
class BusPassengersController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BusPassengers models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new BusPassengersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BusPassengers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BusPassengers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id) {
        $model = new BusPassengers();
        $decParam = base64_decode($id);
        $paramArray = explode(",", $decParam);
        $tripId = $paramArray[0];
        $seatNumber = $paramArray[1];
        $userId = Yii::$app->user->id;        
//        $this->layout = false;
        if ($model->load(Yii::$app->request->post())) {            
//            echo '<pre>';
            $model->seatNumber = implode(", ", $model->seatNumber);
            $model->unitPrice = str_replace(",", "", $model->unitPrice);
            $model->totalPrice = str_replace(",", "", $model->totalPrice);
            $model->totalPaid = str_replace(",", "", $model->totalPaid);
            $model->restPayment = str_replace(",", "", $model->restPayment);
            $model->totalChange = str_replace(",", "", $model->totalChange);
            $model->tripScheduleId = $tripId;
            $userModel = User::find()->where(['id' => $userId])->one();
            $counterId = $userModel->bus_counter_id;
            $counterModel = BusCounter::find()->where(['busCounterId' => $counterId])->one();
            $prefix = $counterModel->counterCode;
            $todate = date('dmy');
            $classCode = $model->getBusClassCode($tripId);
            $ticketCounter = $counterModel->ticketCounter;
            $ticketCounter = str_pad($ticketCounter, 8, "0", STR_PAD_LEFT);            
            $model->bookingCode = $prefix . '-' . $classCode . '/' . $todate . '-' . $ticketCounter;

            $counterModel->ticketCounter = $counterModel->ticketCounter + 1;
            if ($model->validate()) {
                $model->save();
                $counterModel->save();
//            } else {
//                // validation failed: $errors is an array containing error messages
////                var_dump($model->errors);
//                die();
            }

//            $model->refresh();
//            Yii::$app->response->format = 'json';
//            return ['message' => Yii::t('app','Success Create!'), 'id'=>$model->busPassengerId];
            return $this->redirect(['view', 'id' => $model->busPassengerId]);
        }
        
        return $this->render('create', [
                    'model' => $model,
                    'tripId' => $tripId,
                    'seatNumber' => $seatNumber
        ]);
    }

    /**
     * Updates an existing BusPassengers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->busPassengerId]);
        }

        return $this->renderAjax('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BusPassengers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BusPassengers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BusPassengers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = BusPassengers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
