<?php

namespace backend\controllers;

use Yii;
use DateTime;
use backend\models\TripsSchedule;
use backend\models\BusClass;
use backend\models\BusRoutes;
use backend\models\BusFleet;

class BaseController {

    public function createdAt() {
        $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        return $created_at = $date->format('Y:m:d H:i:s');
    }

    public function createdBy() {
        return $userId = Yii::$app->user->id;
    }

    public function getCurrentDate() {
        $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        return $date->format('Y-m-d');
    }

    public function getCurrentTime() {
        $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        return $date->format('H:i:s');
    }

    public function updateTripStatus() {
        $sql = 'UPDATE trips_schedule SET tripStatus = "9" WHERE TIMESTAMPDIFF(MINUTE, departureTime, NOW()) >= 30';
        Yii::$app->db->createCommand($sql)->execute();
    }

    public function selectSeat($id) {
        $getSeat = Yii::$app->db->createCommand("SELECT * FROM seat_passenger WHERE tripScheduleId = '$id'")->queryAll();
//        $arraySeat = array_column($getSeat, 'seatPassengerId');
        $count = count($getSeat[0]);
        $arraySeat = [];
        $j = 1;
        for($i = 0; $i < $count; $i++){
            if($i > 1 && $i < 44){
                $val = $getSeat[0]['seat-'.$j];
                if($val == 0){
                    $key = 'Seat ' . $j;
                    $arraySeat[$j] = $key;
                }                
                $j++;
            }
        }
        return $arraySeat;        
    }

    public function getUnitPrice($id){
        $modelTripSchedule = new TripsSchedule();
        $tripData = $modelTripSchedule->find()->where(['tripScheduleId' => $id])->one();

        $routeId = $tripData->busRouteId;
//        $fleetId = $trip->busFleetId;
//        
//        $modelFleet = new BusFleet();
//        $fleetData = $modelFleet->find()->where(['busFleetId' => $fleetId])->one();
//        $fleetClassId = $fleetData->busClassId;
        
        $modelRoute = new BusRoutes();
        $routeData = $modelRoute->find()->where(['busRouteId' => $routeId])->one();
        $unitPrice = $routeData->ticketPrice;
        
        return $unitPrice;
    }
}
