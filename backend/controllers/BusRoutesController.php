<?php

namespace backend\controllers;

use Yii;
use backend\models\BusRoutes;
use backend\controllers\BaseController;
use backend\search\BusRoutesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BusRoutesController implements the CRUD actions for BusRoutes model.
 */
class BusRoutesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BusRoutes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BusRoutesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BusRoutes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);        
        $base = new BaseController();
        if ($model->load(Yii::$app->request->post())) {
            $model->ticketPrice = str_replace(".", "", $model->ticketPrice);
            
            $model->updated_at = $base->createdAt();
            $model->updated_by = $base->createdBy();
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->busRouteId]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new BusRoutes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BusRoutes();
        $base = new BaseController();
        
        if ($model->load(Yii::$app->request->post())) {
            $model->busDepartureId = 51;
            $model->ticketPrice = str_replace(".", "", $model->ticketPrice);
            
            $model->created_at = $base->createdAt();
            $model->created_by = $base->createdBy();
            
            $model->save(false);
//            echo '<pre>';
//            print_r($model);
//            die();
            return $this->redirect(['view', 'id' => $model->busRouteId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BusRoutes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->busRouteId]);
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//        ]);
//    }

    /**
     * Deletes an existing BusRoutes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BusRoutes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BusRoutes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BusRoutes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
