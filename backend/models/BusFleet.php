<?php

namespace backend\models;

use Yii;
use backend\models\BusClass;

/**
 * This is the model class for table "bus_fleet".
 *
 * @property int $busFleetId
 * @property int $busClassId
 * @property string $busLicensePlate
 * @property int $busSeatsCount
 * @property string $busFleetNote
 * @property int $busFleetStatus
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class BusFleet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bus_fleet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['busClassId', 'busLicensePlate', 'busSeatsCount', 'busFleetStatus'], 'required'],
            [['created_at', 'updated_at','busFleetStatus'], 'safe'],
            [['busClassId', 'busSeatsCount', 'busFleetStatus', 'created_by', 'updated_by'], 'integer'],
            [['busLicensePlate'], 'string', 'max' => 16],
            [['busFleetNote'], 'string', 'max' => 256],
            ['busLicensePlate', 'match', 'pattern' => '/^[B][BK]?\s[0-9]{4}\s[A-Z]{2,3}$/', 'message' => "License Plate should only contain letters and numbers, and 'space' e.g: BK 7001 IN"],
            ['busSeatsCount', 'match', 'pattern' => '/^(4[0-2]|3[0-9]|2[3-9])$/', 'message' => "The number of seats should be in range 23-42 e.g: 36"],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'busFleetId' => 'Fleet ID',
            'busClassId' => 'Class',
            'busLicensePlate' => 'License Plate',
            'busSeatsCount' => 'Seats',
            'busFleetNote' => 'Note',
            'busFleetStatus' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getBusClass() {
        return $this->hasOne(BusClass::className(), ['busClassId' => 'busClassId']);
    }
}
