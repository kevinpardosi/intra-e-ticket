<?php

namespace backend\models;

use Yii;
use backend\models\BusFleet;
use backend\models\BusRoutes;
use backend\models\BusDrivers;
use backend\models\BusPassengers;
use backend\models\SystemStatus;

/**
 * This is the model class for table "trips_schedule".
 *
 * @property int $tripScheduleId
 * @property string $tripNumber
 * @property int $busFleetId
 * @property int $busRouteId
 * @property string $departureTime
 * @property string $departureDate
 * @property int $tripStatus
 * @property int $isBooked
 * @property int $busDriverId
 * @property int $busCoDriverId
 * @property resource $tripScheduleNote
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class TripsSchedule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trips_schedule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['busFleetId', 'busRouteId', 'departureTime', 'departureDate', 'tripStatus', 'isBooked'], 'required'],
            [['busDriverId', 'busCoDriverId', 'busFleetId', 'busRouteId', 'tripStatus', 'isBooked', 'created_by', 'updated_by'], 'integer'],
            [['departureTime', 'departureDate', 'created_at', 'updated_at'], 'safe'],
            [['tripNumber'], 'string', 'max' => 32],
            [['tripScheduleNote'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tripScheduleId' => 'Trip Schedule ID',
            'tripNumber' => 'Trip Number',
            'busFleetId' => 'Fleet',
            'busRouteId' => 'Route',
            'departureTime' => 'Dep. Time',
            'departureDate' => 'Dep. Date',
            'busDriverId' => 'Driver',
            'busCoDriverId' => 'Co-Driver',
            'tripStatus' => 'Status',
            'isBooked' => 'Booked',
            'tripScheduleNote' => 'Trip Schedule Note',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function beforeSave($insert) {
        if ($insert) {
            $this->departureDate = Yii::$app->formatter->asDate(strtotime($this->departureDate), "php:Y-m-d");
            $this->departureTime = Yii::$app->formatter->asTime(strtotime($this->departureTime), "php:H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    public function getBusFleet() {
        return $this->hasOne(BusFleet::className(), ['busFleetId' => 'busFleetId']);
    }
    
    public function getBusRoute() {
          return $this->hasOne(BusRoutes::className(), ['busRouteId' => 'busRouteId']);
    }
    
    public function getBusDriver() {
          return $this->hasOne(BusDrivers::className(), ['busDriverId' => 'busDriverId']);
    }
    
    public function getBusCoDriver() {
          return $this->hasOne(BusDrivers::className(), ['busDriverId' => 'busCoDriverId']);
    }
    
    public function getBusStatus() {
          return $this->hasOne(SystemStatus::className(), ['systemStatusId' => 'tripStatus']);
    }
    
    public function getBusIsBooked() {
          return $this->hasOne(SystemStatus::className(), ['systemStatusId' => 'isBooked']);
    }
}
