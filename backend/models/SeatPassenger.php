<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "seat_passenger".
 *
 * @property int $seatPassengerId
 * @property int $tripScheduleId
 * @property int $seat-1
 * @property int $seat-2
 * @property int $seat-3
 * @property int $seat-4
 * @property int $seat-5
 * @property int $seat-6
 * @property int $seat-7
 * @property int $seat-8
 * @property int $seat-9
 * @property int $seat-10
 * @property int $seat-11
 * @property int $seat-12
 * @property int $seat-13
 * @property int $seat-14
 * @property int $seat-15
 * @property int $seat-16
 * @property int $seat-17
 * @property int $seat-18
 * @property int $seat-19
 * @property int $seat-20
 * @property int $seat-21
 * @property int $seat-22
 * @property int $seat-23
 * @property int $seat-24
 * @property int $seat-25
 * @property int $seat-26
 * @property int $seat-27
 * @property int $seat-28
 * @property int $seat-29
 * @property int $seat-30
 * @property int $seat-31
 * @property int $seat-32
 * @property int $seat-33
 * @property int $seat-34
 * @property int $seat-35
 * @property int $seat-36
 * @property int $seat-37
 * @property int $seat-38
 * @property int $seat-39
 * @property int $seat-40
 * @property int $seat-41
 * @property int $seat-42
 * @property int $seat-43
 * @property int $seat-44
 * @property int $isFull
 * @property int $maintenanceSeat-1
 * @property int $maintenanceSeat-2
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class SeatPassenger extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seat_passenger';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tripScheduleId', 'seat-1', 'seat-2', 'seat-3', 'seat-4', 'seat-5', 'seat-6', 'seat-7', 'seat-8', 'seat-9', 'seat-10', 'seat-11', 'seat-12', 'seat-13', 'seat-14', 'seat-15', 'seat-16', 'seat-17', 'seat-18', 'seat-19', 'seat-20', 'seat-21', 'seat-22', 'seat-23', 'seat-24', 'seat-25', 'seat-26', 'seat-27', 'seat-28', 'seat-29', 'seat-30', 'seat-31', 'seat-32', 'seat-33', 'seat-34', 'seat-35', 'seat-36', 'seat-37', 'seat-38', 'seat-39', 'seat-40', 'seat-41', 'seat-42', 'seat-43', 'seat-44', 'isFull', 'maintenanceSeat-1', 'maintenanceSeat-2', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'seatPassengerId' => 'Seat Passenger ID',
            'tripScheduleId' => 'Trip Schedule ID',
            'seat-1' => 'Seat 1',
            'seat-2' => 'Seat 2',
            'seat-3' => 'Seat 3',
            'seat-4' => 'Seat 4',
            'seat-5' => 'Seat 5',
            'seat-6' => 'Seat 6',
            'seat-7' => 'Seat 7',
            'seat-8' => 'Seat 8',
            'seat-9' => 'Seat 9',
            'seat-10' => 'Seat 10',
            'seat-11' => 'Seat 11',
            'seat-12' => 'Seat 12',
            'seat-13' => 'Seat 13',
            'seat-14' => 'Seat 14',
            'seat-15' => 'Seat 15',
            'seat-16' => 'Seat 16',
            'seat-17' => 'Seat 17',
            'seat-18' => 'Seat 18',
            'seat-19' => 'Seat 19',
            'seat-20' => 'Seat 20',
            'seat-21' => 'Seat 21',
            'seat-22' => 'Seat 22',
            'seat-23' => 'Seat 23',
            'seat-24' => 'Seat 24',
            'seat-25' => 'Seat 25',
            'seat-26' => 'Seat 26',
            'seat-27' => 'Seat 27',
            'seat-28' => 'Seat 28',
            'seat-29' => 'Seat 29',
            'seat-30' => 'Seat 30',
            'seat-31' => 'Seat 31',
            'seat-32' => 'Seat 32',
            'seat-33' => 'Seat 33',
            'seat-34' => 'Seat 34',
            'seat-35' => 'Seat 35',
            'seat-36' => 'Seat 36',
            'seat-37' => 'Seat 37',
            'seat-38' => 'Seat 38',
            'seat-39' => 'Seat 39',
            'seat-40' => 'Seat 40',
            'seat-41' => 'Seat 41',
            'seat-42' => 'Seat 42',
            'seat-43' => 'Seat 43',
            'seat-44' => 'Seat 44',
            'isFull' => 'Is Full',
            'maintenanceSeat-1' => 'Maintenance Seat 1',
            'maintenanceSeat-2' => 'Maintenance Seat 2',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function checkSeat($id, $seat){               
        $model = SeatPassenger::find()->where(['tripScheduleId' => $id])->one();
        $col = 'seat-'.$seat;
        $status = $model->$col; 
        if($status == 0){
            $status = 'seat-available';
        } else if($status == 1){
            $status = 'seat-occupied';
        }
        return $status;        
    }
}
