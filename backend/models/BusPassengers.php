<?php

namespace backend\models;

use Yii;
use backend\models\BusRoutes;
use backend\models\BusClass;
use backend\models\BusFleet;
use backend\models\TripsSchedule;
use backend\models\Areas;

/**
 * This is the model class for table "bus_passengers".
 *
 * @property int $busPassengerId
 * @property int $tripScheduleId
 * @property string $busPassengerName
 * @property string $busPassengerAddress
 * @property string $busPassengerPhone
 * @property string $unitPrice
 * @property int $totalSeats
 * @property string $totalPrice
 * @property string $totalPaid
 * @property string $totalChange
 * @property string $restPayment
 * @property int $isMultiSeat
 * @property string $bookingCode
 * @property string $seatNumber
 * @property int $isLuggage
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class BusPassengers extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'bus_passengers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['tripScheduleId', 'unitPrice', 'totalSeats', 'totalPrice', 'totalPaid', 'restPayment', 'seatNumber', 'totalChange'], 'required'],
            [['tripScheduleId', 'totalSeats', 'isMultiSeat', 'isLuggage', 'created_by', 'updated_by'], 'integer'],
            [['unitPrice', 'totalPrice', 'totalPaid', 'restPayment', 'totalChange'], 'number', 'numberPattern' => '/[0-9]|\./'],
            [['created_at', 'updated_at'], 'safe'],
            [['busPassengerName'], 'string', 'max' => 64],
            [['busPassengerAddress', 'seatNumber'], 'string', 'max' => 128],
            [['busPassengerPhone'], 'string', 'max' => 16],
            [['bookingCode'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'busPassengerId' => 'Bus Passenger ID',
            'tripScheduleId' => 'Trip Schedule ID',
            'busPassengerName' => 'Bus Passenger Name',
            'busPassengerAddress' => 'Bus Passenger Address',
            'busPassengerPhone' => 'Bus Passenger Phone',
            'unitPrice' => 'Unit Price',
            'totalSeats' => 'Seats',
            'totalPrice' => 'Total',
            'totalPaid' => 'Cash',
            'totalChange' => 'Change',
            'restPayment' => 'Rest Payment',
            'isMultiSeat' => 'Multi Seat',
            'bookingCode' => 'Booking Code',
            'seatNumber' => 'Seat Number',
            'isLuggage' => 'Luggage',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getTrip() {
          return $this->hasOne(TripsSchedule::className(), ['tripScheduleId' => 'tripScheduleId']);
    }
    
    public function getRouteName($id){
        $modelRoute = BusRoutes::find()->where(['busRouteId' => $id])->one();
        $dep = $modelRoute->busDepartureId;
        $des = $modelRoute->busDestinationId;
        
        $modelAreaDep = Areas::find()->where(['areaId' => $dep])->one();
        $depName = $modelAreaDep->areaName;
        $modelAreaDes = Areas::find()->where(['areaId' => $des])->one();
        $desName = $modelAreaDes->areaName;
        
        return $depName . ' - ' . $desName;
    }
    
    public function getDepartureAt($id){
        $modelTrip = TripsSchedule::find()->where(['tripScheduleId' => $id])->one();
        $date = date('D, d-m-Y', strtotime($modelTrip->departureDate));
        $time = date('G:i', strtotime($modelTrip->departureTime));
        
        return $date . ' - ' . $time;
    }
    
    public function getBusClassCode($id){
        $modelTrip = TripsSchedule::find()->where(['tripScheduleId' => $id])->one();
        $fleetId = $modelTrip->busFleetId;
        $modelBusFleet = BusFleet::find()->where(['busFleetId' => $fleetId])->one();
        $busClassId =  $modelBusFleet->busClassId;
        $modelBusClass = BusClass::find()->where(['busClassId' => $busClassId])->one();
        return $classCode = $modelBusClass->busClassCode;
    }
}