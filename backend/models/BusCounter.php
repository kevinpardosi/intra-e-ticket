<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "bus_counter".
 *
 * @property int $busCounterId
 * @property int $ticketCounter
 * @property string $counterCode
 * @property string $counterName
 * @property string $address
 * @property string $phone
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $isActive
 */
class BusCounter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bus_counter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['counterCode'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by', 'isActive', 'ticketCounter'], 'integer'],
            [['counterCode'], 'string', 'max' => 8],
            [['address'], 'string', 'max' => 64],
            [['phone', 'counterName'], 'string', 'max' => 16],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'busCounterId' => 'Bus Counter ID',
            'ticketCounter' => 'Ticket Counter',
            'counterCode' => 'Counter Code',
            'counterName' => 'Counter Name',
            'address' => 'Address',
            'phone' => 'Phone',            
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'isActive' => 'Is Active',
        ];
    }
}
