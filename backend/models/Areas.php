<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "areas".
 *
 * @property int $areaId
 * @property string $areaName
 * @property int $areaDeptCount
 * @property int $areaDestCount
 * @property string $areaNote
 */
class Areas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'areas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['areaName'], 'required'],
            [['areaDeptCount', 'areaDestCount'], 'integer'],
            [['areaName'], 'string', 'max' => 64],
            [['areaNote'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'areaId' => 'Area ID',
            'areaName' => 'Area Name',
            'areaDeptCount' => 'Area Dept Count',
            'areaDestCount' => 'Area Dest Count',
            'areaNote' => 'Area Note',
        ];
    }
}
