<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "bus_drivers".
 *
 * @property int $busDriverId
 * @property string $busDriverCode
 * @property string $busDriverName
 * @property string $busDriverPhone
 * @property string $busDriverAddress
 * @property string $busDriverNote
 * @property int $driverStatus
 * @property string $licenseExpiredAt
 * @property int $is_expired
 * @property resource $idCard
 * @property resource $drivingLicense
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class BusDrivers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bus_drivers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['busDriverName', 'busDriverPhone', 'busDriverAddress', 'driverStatus'], 'required'],
            [['driverStatus', 'is_expired', 'created_by', 'updated_by'], 'integer'],
            [['licenseExpiredAt', 'created_at', 'updated_at'], 'safe'],
            [['idCard', 'drivingLicense'], 'string'],
            [['busDriverCode', 'busDriverPhone'], 'string', 'max' => 16],
            [['busDriverName'], 'string', 'max' => 32],
            [['busDriverAddress'], 'string', 'max' => 128],
            [['busDriverNote'], 'string', 'max' => 256],
            ['busDriverCode', 'match', 'pattern' => '/^[A-Za-z0-9 -]+$/', 'message' => "Code should only contain letters and numbers, not symbols except '-' and 'space' e.g: Kp - 100"],
            ['busDriverPhone', 'match', 'pattern' => '/(?:\+62)?0?8\d{2}(\d{8})/', 'message' => "Phone should only contain numbers, not symbols except '+' e.g: 081212122121"],
            ['busDriverName', 'match', 'pattern' => '/^[a-z0-9 ,.\-]+$/i', 'message' => "Name should only contain letters and numbers, not symbols except '-', '.', ',', and '/' e.g: Kevin - Pardosi"],
            ['busDriverAddress', 'match', 'pattern' => '/^[\r\na-z0-9 ,.\-]+$/i', 'message' => "Address should only contain letters and numbers, not symbols except '-', '.', ',', and '/' e.g: Jl. Seksama, No. 90 - Medan"],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'busDriverId' => 'ID',
            'busDriverCode' => 'Code',
            'busDriverName' => 'Name',
            'busDriverPhone' => 'Phone',
            'busDriverAddress' => 'Address',
            'busDriverNote' => 'Driver Additional Note',
            'driverStatus' => 'Status',
            'licenseExpiredAt' => 'License Periode',
            'is_expired' => 'Expired',
            'idCard' => 'ID CARD',
            'drivingLicense' => 'Driving License',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
