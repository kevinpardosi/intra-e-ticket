<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "bus_class".
 *
 * @property int $busClassId
 * @property string $busClassCode
 * @property string $busClassName
 * @property string $busClassNote
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class BusClass extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bus_class';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['busClassCode', 'busClassName', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['busClassCode', 'busClassName'], 'string', 'max' => 32],
            [['busClassNote'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'busClassId' => 'Bus Class ID',
            'busClassCode' => 'Bus Class Code',
            'busClassName' => 'Bus Class Name',
            'busClassNote' => 'Bus Class Note',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
