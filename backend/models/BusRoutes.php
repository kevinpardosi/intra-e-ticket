<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;
use backend\models\Areas;
use backend\models\BusClass;

/**
 * This is the model class for table "bus_routes".
 *
 * @property int $busRouteId
 * @property int $busClassId
 * @property int $busDepartureId
 * @property int $busDestinationId
 * @property string $ticketPrice
 * @property int $busRouteCount
 * @property string $busRouteNote
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class BusRoutes extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'bus_routes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['busClassId', 'busDepartureId', 'busDestinationId', 'busRouteCount', 'created_by', 'updated_by'], 'integer'],
            [['busDepartureId', 'busDestinationId', 'ticketPrice', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'required'],
            [['ticketPrice'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['busRouteNote'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'busRouteId' => 'Bus Route ID',
            'busClassId' => 'Bus Class',
            'busDepartureId' => 'Departure',
            'busDestinationId' => 'Destination',
            'ticketPrice' => 'Ticket Price',
            'busRouteCount' => 'Bus Route Count',
            'busRouteNote' => 'Bus Route Note',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function routesList() {
        $routesList = [];

        $routes = BusRoutes::find()->orderBy('busDestinationId')->all();

        $routesList = ArrayHelper::map($routes, 'busRouteId', function ($routes) {
                    return $routes->departureName->areaName . ' - ' . $routes->destinationName->areaName;
                });

        return $routesList;
    }

    public function getDepartureName() {
        return $this->hasOne(Areas::className(), ['areaId' => 'busDepartureId']);
    }

    public function getDestinationName() {
        return $this->hasOne(Areas::className(), ['areaId' => 'busDestinationId']);
    }
    
    public function getBusClass() {
        return $this->hasOne(BusClass::className(), ['busClassId' => 'busClassId']);
    }

}
