<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "system_status".
 *
 * @property int $systemStatusId
 * @property string $statusCode
 * @property string $statusDetail
 */
class SystemStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'system_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['statusCode', 'statusDetail'], 'required'],
            [['statusCode', 'statusDetail'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'systemStatusId' => 'System Status ID',
            'statusCode' => 'Status Code',
            'statusDetail' => 'Status Detail',
        ];
    }
}
