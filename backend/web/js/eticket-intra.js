// adding dot in ticket-price input in create
$(function () {
    if ($('div').is('.bus-routes-form')) {
        $(document).ready(function () {
            $('input#ticket-price').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;
                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                });
            });
        });
    }
});

//validate ticket should be at least 25.000 in create
$(function () {
    if ($('div').is('.bus-routes-form')) {
        $(document).ready(function () {
            document.getElementById("ticket-price").addEventListener("change", myFunction);
            function myFunction() {
                var bruto = $("#ticket-price").val();
                bruto = bruto.split('.').join("");
                if (bruto < 25000) {
                    alert('Ticket Price min. Rp. 25.000,-');
                    $("#ticket-price").val('');
                }
            }
        });
    }
});

// adding dot in ticket-price input in view update
$(function () {
    if ($('div').is('.bus-routes-view')) {
        $(document).ready(function () {
            $('input#busroutes-ticketprice').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;
                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                });
            });
        });
    }
});

//validate ticket should be at least 25.000 in view update
$(function () {
    if ($('div').is('.bus-routes-view')) {
        $(document).ready(function () {
            document.getElementById("busroutes-ticketprice").addEventListener("change", myFunction);
            function myFunction() {
                var bruto = $("#busroutes-ticketprice").val();
                bruto = bruto.split('.').join("");
                if (bruto < 25000) {
                    alert('Ticket Price min. Rp. 25.000,-');
                }
            }
        });
    }
});

//pop-up form add passenger
//$(document).ready(function () {
//    $('#modalButton').click(function () {
//        $('#modal').modal('show')
//                .find('#modalContent')
//                .load($(this).attr('value'));
//    });
//});

//$(function () {
//    $('#modalButton').click(function () {
//        $('#modal').modal('show')
//                .find('#modalContent')
//                .load($(this).attr('value'));
////                window.location = '../bus-passengers/create';
//    });
//});

$(function () {
    if ($('div').is('#seat-row')) {
        $(document).ready(function () {
            let searchParams = new URLSearchParams(window.location.search);
            let tripId = searchParams.get('id');
            $('p[id^=seat-number-]').click(function () {
                var idSeat = $(this).attr('id');
                var seatNumber = $('#' + idSeat).text();
                var param = tripId + "," + seatNumber;
                window.location = '../bus-passengers/create?id=' + btoa(param);
            });
        });
    }
});

//disable dropdown seat based on isMultiSeat checkbox
//$(document).ready(function () {
//    $('#is-multi-seat-id').change(function () {
////        var checkbox = document.getElementById('is-multi-seat-id');
//        let searchParams = new URLSearchParams(window.location.search);
//        let param = searchParams.get('id');
//
////        var statusSeatOptions = '';
//        if (this.checked) {
//            statusSeatOptions = false;
//        } else {
//            statusSeatOptions = true;
//        }
//        document.getElementById('seat-number-options-id').disabled = statusSeatOptions;
//        $('#seat-number-options-id').load('create?id=' + param + ' #seat-number-options-id');
//        return false;
//    });
//});

$(document).ready(function () {
    $('#seat-number-options-id').change(function () {
        let searchParams = new URLSearchParams(window.location.search);
        let param = searchParams.get('id');
        $('#is-multi-seat-id').load('create?id=' + param + ' #is-multi-seat-id', function () {
            var numSeat = $('.select2-selection__choice').length;
            var unitPrice = $("#unit-price-id").val();
            unitPrice = unitPrice.split(',').join("");
            if (numSeat > 1) {
                $('#is-multi-seat-id').prop('checked', true);
                $('#btn-create-ticket-passenger-id').prop('disabled', false);
            } else if (numSeat == 1) {
                $('#is-multi-seat-id').prop('checked', false);
                $('#btn-create-ticket-passenger-id').prop('disabled', false);
            } else {
                $('#is-multi-seat-id').prop('checked', false);
                $('#btn-create-ticket-passenger-id').prop('disabled', true);
                alert('Seat should at least one.')
            }

            var cash = $("#total-paid-id").val();
            cash = cash.split(',').join("");
            var totalPrice = unitPrice * numSeat;
            if (cash !== null) {
                if (cash > totalPrice) {
                    var totalChange = cash - totalPrice;
                    totalChange = totalChange.toLocaleString(undefined, {maximumFractionDigits: 2});
                    document.getElementById('total-change-id').value = totalChange;
                    document.getElementById('rest-payment-id').value = 0;
                } else if (cash < totalPrice) {
                    var restPayment = totalPrice - cash;
                    restPayment = restPayment.toLocaleString(undefined, {maximumFractionDigits: 2});
                    document.getElementById('rest-payment-id').value = restPayment;
                    document.getElementById('total-change-id').value = 0;
                } else {
                    document.getElementById('rest-payment-id').value = 0;
                    document.getElementById('total-change-id').value = 0;
                }
            }
            totalPrice = totalPrice.toLocaleString(undefined, {maximumFractionDigits: 2});
            document.getElementById('total-price-id').value = totalPrice;
            document.getElementById('total-seat-id').value = numSeat;
        });
    });
});

$(document).ready(function () {
    $('#total-paid-id').change(function () {
        var cash = $("#total-paid-id").val();
        var totalPrice = $("#total-price-id").val();
        cash = cash.split(',').join("");
        totalPrice = totalPrice.split(',').join("");
        if (cash > totalPrice) {
            var totalChange = cash - totalPrice;
            totalChange = totalChange.toLocaleString(undefined, {maximumFractionDigits: 2});
            document.getElementById('total-change-id').value = totalChange;
            document.getElementById('rest-payment-id').value = 0;
        } else if (cash < totalPrice) {
            var restPayment = totalPrice - cash;
            restPayment = restPayment.toLocaleString(undefined, {maximumFractionDigits: 2});
            document.getElementById('rest-payment-id').value = restPayment;
            document.getElementById('total-change-id').value = 0;
        } else {
            document.getElementById('rest-payment-id').value = 0;
            document.getElementById('total-change-id').value = 0;
        }
    });
});

$(document).ready(function () {
    if ($('div').is('.bus-passengers-form')) {
        $('#is-luggage-id').prop('checked', false);
        var unitPrice = $("#unit-price-id").val();
        unitPrice = unitPrice.split(',').join("");
        var totalPrice = unitPrice * 1;
        totalPrice = totalPrice.toLocaleString(undefined, {maximumFractionDigits: 2});
        document.getElementById('total-price-id').value = totalPrice;
//        document.getElementById('unit-price-id').value = totalPrice;
    }
});

$(function () {
    if ($('div').is('.bus-passengers-form')) {
        $(document).ready(function () {
            $('input#total-paid-id').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                            ;
                });
            });
        });
    }
});

//window.onload = function () {
//    if (window.jQuery) {
//        // jQuery is loaded 
////        var unitPrice = $("#unit-price-id").val();
////        alert("Yeah!");
////        alert(unitPrice);
//    } else {
//        // jQuery is not loaded
//        alert("Doesn't Work");
//    }
//}

//function setOccupied() {    
//        var seatArray = $('#set-occupied-id').text(); 
//        seatArray = JSON.parse(seatArray);
//        $.each(seatArray, function(i, elm){
//            $('#seat-number-' + elm).parent().removeClass('seat-available').addClass('seat-available');
//        });        
//}