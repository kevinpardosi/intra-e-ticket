<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TripsSchedule */

$this->title = 'Update Trips Schedule: ' . $model->tripScheduleId;
$this->params['breadcrumbs'][] = ['label' => 'Trips Schedules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tripScheduleId, 'url' => ['view', 'id' => $model->tripScheduleId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trips-schedule-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
