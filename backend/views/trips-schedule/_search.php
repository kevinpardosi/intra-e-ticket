<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\search\TripsScheduleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trips-schedule-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tripScheduleId') ?>

    <?= $form->field($model, 'tripNumber') ?>

    <?= $form->field($model, 'busFleetId') ?>

    <?= $form->field($model, 'busRouteId') ?>

    <?= $form->field($model, 'departureTime') ?>

    <?php // echo $form->field($model, 'departureDate') ?>

    <?php // echo $form->field($model, 'tripStatus') ?>

    <?php // echo $form->field($model, 'isBooked') ?>

    <?php // echo $form->field($model, 'tripScheduleNote') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
