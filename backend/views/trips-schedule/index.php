<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\search\TripsScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'TRIPS SCHEDULE';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trips-schedule-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Add Trip', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '5px',
                ],
            ],
            [
                'attribute' => 'tripNumber',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '80px',
                ],
            ],
            [
                'attribute' => 'busFleetId',
                'value' => function ($model) {
                    return $model->busFleet->busLicensePlate;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '80px',
                ],
            ],
            [
                'attribute' => 'busRouteId',
                'value' => function ($model) {
                    return $model->busRoute->departureName->areaName
                            . ' - '
                            . $model->busRoute->destinationName->areaName;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'departureTime',
                'value' => function ($data) {
                    return date('H:i', strtotime($data->departureTime)) . ' WIB';
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '70px',
                ],
            ],
            [
                'attribute' => 'departureDate',
                'value' => function ($data) {
                    return date('d-m-Y', strtotime($data->departureDate));
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '70px',
                ],
            ],
            [
                'attribute' => 'busFleetId',
                'label' => 'Class',
                'value' => function ($model) {
                    return $model->busFleet->busClass->busClassName;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '90px',
                ],
            ],
            [
                'attribute' => 'busDriverId',
                'value' => function ($model) {
                    return $model->busDriver->busDriverName;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '140px',
                ],
            ],
            [
                'attribute' => 'busCoDriverId',
                'value' => function ($model) {
                    return $model->busCoDriver->busDriverName;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '140px',
                ],
            ],
            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'options' => [
                    'width' => 65
                ],
            ],
        ],
    ]);
    ?>


</div>
