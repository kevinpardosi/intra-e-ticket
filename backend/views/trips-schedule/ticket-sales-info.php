<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use backend\models\SeatPassenger;

/* @var $this yii\web\View */
/* @var $modelTrip backend\models\TripsSchedule */

$this->title = 'TRIP - ' . $modelTrip->tripNumber;
$this->params['breadcrumbs'][] = ['label' => 'Trips Schedules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$modelSeat = new SeatPassenger();
?>

<!--<script type="text/javascript">
    function showModal() {
        $('#passenger_modal').modal('show');
        $('#passenger_modal_content').load("<?= Yii::getAlias('@web') ?>/intra/admin/bus-passengers/create");
    }
</script>-->

<!--<p>
<?= Html::button('Add Schedule', ['class' => 'btn btn-success', 'onClick' => 'showModal()']) ?>
</p>-->

<?php
//Modal::begin([
//    'header' => '<h4>Add Passenger</h4>',
//    'id' => 'passenger_modal',
//    'size' => 'modal-lg',
//]);
//
//echo '<div id="passenger_modal_content"><div>';
//Modal::end();
?>

<div class="flex-body">
    <div class="flex-row">
        <div class="col-lg-8">
            <div id="seat-row">
                <div class="corridor"></div>
                <div class="corridor"></div>
                <div class="corridor"></div>
                <div class="driver-steer">
                    <img width="45" height="45" src="<?= Yii::getAlias('@web') ?>/dist/img/steer.png">
                </div>                
            </div>
            <div id="seat-row">
                <div class="driver-side">
                    CO - DRIVER
                </div>
                <div class="corridor"></div>
                <div class="driver-side">
                    DRIVER
                </div>
            </div>
            <div id="seat-row">
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 1); ?>">
                    <p id="seat-number-1" class="seat-number">1</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 2); ?>">
                    <p id="seat-number-2" class="seat-number">2</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>            
                <div class="corridor"></div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 3); ?>">
                    <p id="seat-number-3" class="seat-number">3</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 4); ?>">
                    <p id="seat-number-4" class="seat-number">4</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
            </div>
            <div id="seat-row">
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 5); ?>">
                    <p id="seat-number-5" class="seat-number">5</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 6); ?>">
                    <p id="seat-number-6" class="seat-number">6</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>            
                <div class="corridor"></div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 7); ?>">
                    <p id="seat-number-7" class="seat-number">7</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 8); ?>">
                    <p id="seat-number-8" class="seat-number">8</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
            </div>
            <div id="seat-row">
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 9); ?>">
                    <p id="seat-number-9" class="seat-number">9</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 10); ?>">
                    <p id="seat-number-10" class="seat-number">10</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>            
                <div class="corridor"></div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 11); ?>">
                    <p id="seat-number-11" class="seat-number">11</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 12); ?>">
                    <p id="seat-number-12" class="seat-number">12</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
            </div>
            <div id="seat-row">
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 13); ?>">
                    <p id="seat-number-13" class="seat-number">13</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 14); ?>">
                    <p id="seat-number-14" class="seat-number">14</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>            
                <div class="corridor"></div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 15); ?>">
                    <p id="seat-number-15" class="seat-number">15</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 16); ?>">
                    <p id="seat-number-16" class="seat-number">16</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
            </div>
            <div id="seat-row">
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 17); ?>">
                    <p id="seat-number-17" class="seat-number">17</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 18); ?>">
                    <p id="seat-number-18" class="seat-number">18</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>            
                <div class="corridor"></div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 19); ?>">
                    <p id="seat-number-19" class="seat-number">19</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 20); ?>">
                    <p id="seat-number-20" class="seat-number">20</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
            </div>
            <div id="seat-row">
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 21); ?>">
                    <p id="seat-number-21" class="seat-number">21</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 22); ?>">
                    <p id="seat-number-22" class="seat-number">22</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>            
                <div class="corridor"></div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 23); ?>">
                    <p id="seat-number-23" class="seat-number">23</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 24); ?>">
                    <p id="seat-number-24" class="seat-number">24</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
            </div>
            <div id="seat-row">
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 25); ?>">
                    <p id="seat-number-25" class="seat-number">25</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 26); ?>">
                    <p id="seat-number-26" class="seat-number">26</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>            
                <div class="corridor"></div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 27); ?>">
                    <p id="seat-number-27" class="seat-number">27</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 28); ?>">
                    <p id="seat-number-28" class="seat-number">28</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
            </div>
            <div id="seat-row">
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 29); ?>">
                    <p id="seat-number-29" class="seat-number">29</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 30); ?>">
                    <p id="seat-number-30" class="seat-number">30</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>            
                <div class="corridor"></div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 31); ?>">
                    <p id="seat-number-31" class="seat-number">31</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 32); ?>">
                    <p id="seat-number-32" class="seat-number">32</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
            </div>
            <div id="seat-row">
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 33); ?>">
                    <p id="seat-number-33" class="seat-number">33</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 34); ?>">
                    <p id="seat-number-34" class="seat-number">34</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>            
                <div class="corridor"></div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 35); ?>">
                    <p id="seat-number-35" class="seat-number">35</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 36); ?>">
                    <p id="seat-number-36" class="seat-number">36</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
            </div>
            <div id="seat-row">
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 37); ?>">
                    <p id="seat-number-37" class="seat-number">37</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 38); ?>">
                    <p id="seat-number-38" class="seat-number">38</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>            
                <div class="corridor"></div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 39); ?>">
                    <p id="seat-number-39" class="seat-number">39</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 40); ?>">
                    <p id="seat-number-40" class="seat-number">40</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
            </div>
            <div id="seat-row">
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 41); ?>">
                    <p id="seat-number-41" class="seat-number">41</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 42); ?>">
                    <p id="seat-number-42" class="seat-number">42</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 43); ?>">
                    <p id="seat-number-43" class="seat-number">43</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
                <div class="<?php echo $modelSeat->checkSeat($modelTrip->tripScheduleId, 44); ?>">
                    <p id="seat-number-44" class="seat-number">44</p>
                    <img class="passenger-seat" src="<?= Yii::getAlias('@web') ?>/dist/img/seat.png">
                </div>
            </div>
        </div>
    </div>
    <div class="flex-column">
        <div>

            <?php
            $attributes = [
                [
                    'columns' => [
                        [
                            'attribute' => 'busRouteId',
                            'label' => 'Route',
                            'value' => function ($form, $widget) {
                                $model = $widget->model;
                                return $model->busRoute->departureName->areaName
                                        . ' - '
                                        . $model->busRoute->destinationName->areaName;
                            },
                        ],
                        [
                            'attribute' => 'departureTime',
                            'label' => 'Departure Time',
                            'value' => function ($form, $widget) {
                                $model = $widget->model;
                                return date('H:i', strtotime($model->departureTime)) . ' WIB';
                            },
                        ],
                    ],
                ],
                    ]
            ?>

            <?=
            DetailView::widget([
                'model' => $modelTrip,
                'id' => $modelTrip->tripScheduleId,
                'responsive' => true,
                'enableEditMode' => false,
                'condensed' => true,
                'hover' => true,
                'mode' => DetailView::MODE_VIEW,
                'panel' => [
                    'heading' => 'TRIP NUMBER # ' . $modelTrip->tripNumber . '  ( ' . $modelTrip->busFleet->busLicensePlate . ' )'
                    . ' - ' . $modelTrip->busDriver->busDriverName,
                    'type' => DetailView::TYPE_INFO,
//                    'footer' => '',
//                    'footerOptions' => [
//                        'template' => '{buttons}',
//                    ]                    
                ],
                'mainTemplate' => '{detail}',
                'attributes' => $attributes,
            ])
            ?>

        </div>
        <div>
            <?php
            echo GridView::widget([
                'dataProvider' => $dataProviderPassenger,
                'options' => ['style' => 'font-size:12px;'],
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => [
                        'id' => '-pjax',
                        'enableReplaceState' => false,
                        'enablePushState' => false,
                    ],
                ],
                'striped' => true,
                'hover' => true,
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'tableOptions' => ['class' => 'table table-hover'],
                'resizableColumns' => true,
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn',
                        'header' => 'No',
                        'options' => [
                            'width' => '5px',
                        ],
                    ],
                    [
                        'attribute' => 'bookingCode',
                        'header' => 'Booking Code',
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                        'options' => [
                            'width' => '100px',
                        ],
                    ],
                    [
                        'attribute' => 'busPassengerName',
                        'header' => 'Name',
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                        'options' => [
                            'width' => '100px',
                        ],
                    ],
                    [
                        'attribute' => 'busPassengerPhone',
                        'header' => 'Phone',
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                        'options' => [
                            'width' => '100px',
                        ],
                    ],
                    [
                        'attribute' => 'seatNumber',
                        'header' => 'Seat',
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                        'options' => [
                            'width' => '100px',
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</div>