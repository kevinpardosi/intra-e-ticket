<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TripsSchedule */

$this->title = 'TRIP - ADD NEW';
$this->params['breadcrumbs'][] = ['label' => 'Trips Schedule', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trips-schedule-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
