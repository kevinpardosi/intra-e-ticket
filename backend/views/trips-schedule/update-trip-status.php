<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Trip Schedule */
?>

<h4 align="center">UPDATE TRIP STATUS</h4>

<?php $form = ActiveForm::begin(['layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
            'error' => '',
            'hint' => true,
        ],
    ],
    ]); ?>

<!--<input type="text" id="choosen-id" value="">--> 
<div class="form-group">
    <div class="col-sm-offset-5">
<?= Html::submitButton('Pilih', ['class'=> 'btn btn-primary']) ?>
<?php
echo "&nbsp";
echo "&nbsp"; 
echo Html::a('Keluar', ['index'],[
	'class'=>'btn btn-danger',
	'onclick' =>'$("#modal").modal("hide");
	return false;'
	]);
?>
    </div>
</div>
<?php ActiveForm::end();?>