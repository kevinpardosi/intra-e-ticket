<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\ButtonDropdown;

/* @var $this yii\web\View */
/* @var $searchModel backend\search\TripsScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "TODAY'S TRIPS";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="trips-schedule-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>   

    <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => false,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '5px',
                ],
            ],
            [
                'attribute' => 'tripNumber',
                'header' => 'Trip <br> Number',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '100px',
                ],
            ],
            [
                'attribute' => 'busFleetId',
                'header' => 'Bus <br> Plate',
                'value' => function ($model) {
                    return $model->busFleet->busLicensePlate;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '85px',
                ],
            ],
            [
                'attribute' => 'busRouteId',
                'header' => 'Route',
                'value' => function ($model) {
                    return $model->busRoute->departureName->areaName
                            . ' - '
                            . $model->busRoute->destinationName->areaName;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'departureTime',
                'header' => 'Dept. At',
                'value' => function ($data) {
                    return date('H:i', strtotime($data->departureTime)) . ' WIB';
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '70px',
                ],
            ],
            [
                'attribute' => 'busFleetId',
                'header' => 'Class',
                'value' => function ($model) {
                    return $model->busFleet->busClass->busClassName;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '95px',
                ],
            ],
            [
                'attribute' => 'busDriverId',
                'header' => 'Driver',
                'value' => function ($model) {
                    return $model->busDriver->busDriverName;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '140px',
                ],
            ],
            [
                'attribute' => 'tripStatus',
                'header' => 'Status',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->tripStatus == 9) {
                        $label = '<i class="glyphicon glyphicon-flag" style="color: #ff0000"></i>';
                    } else {
                        $label = '<i class="glyphicon glyphicon-flag" style="color: #0059b3"></i>';
                    }
                    return Html::a($label, ['update-trip-status', 'id' => $model->tripScheduleId],
                        [
                            'data' => [
                                'confirm' => 'Are you sure you want to update Trip Status?',
                                'method' => 'post',
                            ],
                        ]
                    );
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '30px',
                ],
            ],
            [
                'attribute' => 'isBooked',
                'header' => 'Booked',
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data->isBooked == 8) {
                        return '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                    } else {
                        return '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                    }
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '35px',
                ],
            ],
            ['class' => '\kartik\grid\ActionColumn',
                'header' => 'Action',
                'width' => '116px',
                'headerOptions' => [
//                    'width' => '136',
                    'style' => 'color:#BD2B2B; text-align:center'
                ],
                'contentOptions' => ['vertical-align: middle;'],
                'template' => '{all}',
                'buttons' => [
                    'all' => function ($url, $model, $key) {
                        $visibleBool = false;
                        if ($model['tripStatus'] == 10) {
                            $visibleBool = true;
                        }
                        return ButtonDropdown::widget([
                                    'encodeLabel' => true,
                                    'label' => 'Options',
                                    'dropdown' => [
                                        'encodeLabels' => false,
                                        'items' => [
                                            [
                                                'label' => Yii::t('app', '<i class="glyphicon glyphicon-shopping-cart"></i> Sell Tickets'),
                                                'url' => ['ticket-sales-info', 'id' => $key],
                                                'visible' => $visibleBool
                                            ],
                                            [
                                                'label' => Yii::t('app', '<i class="glyphicon glyphicon-print"></i> Print Trip Permit'),
                                                'url' => ['print-trip-permit', 'id' => $key],
                                                'visible' => $visibleBool,
                                            ],
                                            [
                                                'label' => \Yii::t('app', '<i class="glyphicon glyphicon-remove-sign"></i> Delete'),
                                                'linkOptions' => [
                                                    'data' => [
                                                        'method' => 'post',
                                                        'confirm' => \Yii::t('yii', 'Are you sure you want to delete this item?'),
                                                    ],
                                                ],
                                                'url' => ['delete', 'id' => $key],
                                                'visible' => true,
                                            ],
                                        ],
                                        'options' => [
                                            'class' => 'dropdown-menu-right',
                                        ],
                                    ],
                                    'options' => [
                                        'class' => 'btn-default',
                                    ],
                                    'split' => true,
                        ]);
                    }
                ]
            ],
        ],
    ]);
    ?>
</div>
