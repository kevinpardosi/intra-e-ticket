<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\BusDrivers;
use backend\models\BusRoutes;
use backend\models\BusFleet;
use kartik\select2\Select2;
use kartik\builder\Form;

$form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);

/* @var $this yii\web\View */
/* @var $model backend\models\TripsSchedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trips-schedule-form">

    <div class="bus-fleet-header"><h2>FORM - ADD TRIP</h2></div>   

    <div class="col-sm-12">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'compactGrid' => true,
            'attributes' => [
                'tripNumber' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Tanda Nomor Kendaraan Bermotor...']],
                'busFleetId' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\select2\Select2',
                    'options' => [
                        'data' => ArrayHelper::map(BusFleet::find()->all(), 'busFleetId', 'busLicensePlate'),
                        'value' => null,
                    ],
                    'hint' => 'Pilih Armada'
                ],
                'busRouteId' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\select2\Select2',
                    'options' => [
                        'data' => BusRoutes::routeslist(),
                        'value' => null,
                    ],
                    'hint' => 'Pilih Rute'
                ]
            ]
        ]);
        ?>    
    </div>

    <div class="col-sm-3">
        <?php
        echo '<label class="control-label">Driver</label>';
        echo Select2::widget([
            'model' => $model,
            'attribute' => 'busDriverId',
            'data' => ArrayHelper::map(BusDrivers::find()->all(), 'busDriverId', 'busDriverName'),
            'options' => [
                'placeholder' => 'Select Driver...',
            ],
            'pluginOptions' => [
                'allowClear' => false
            ],
        ]);
        ?>
    </div>
    <div class="col-sm-3">
        <?php
        echo '<label class="control-label">Co - Driver</label>';
        echo Select2::widget([
            'model' => $model,
            'attribute' => 'busCoDriverId',
            'data' => ArrayHelper::map(BusDrivers::find()->all(), 'busDriverId', 'busDriverName'),
            'options' => [
                'placeholder' => 'Select Co - Driver...',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-sm-6">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'compactGrid' => true,
            'attributes' => [
                'departureTime' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\time\TimePicker',
                    'hint' => 'Departure at (hh:mm)',
                    'options' => [
                        'pluginOptions' => [
                            'showMeridian' => false,
                            'minuteStep' => 1,
                        ],
                    ]
                ],
                'departureDate' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\date\DatePicker',
                    'hint' => 'Departure on (dd/mm/yyyy)',
                    'options' => [
                        'options' => ['value' => date('d-m-Y', strtotime(date('Y-m-d')))],                        
                        'pluginOptions' => [                            
                            'autoclose' => true,
                            'startDate' => date('d-m-Y', strtotime(date('Y-m-1'))),
                            'format' => 'dd-mm-yyyy',
                        ]
                    ]
                ],
            ]
        ]);
        ?>
    </div>

    <div class="col-sm-12">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'compactGrid' => true,
            'attributes' => [
                'tripScheduleNote' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Insert the trip additional note, e.g: Bus ini diisi oleh rombongan...']],
            ]
        ]);

        echo '<div class="text-right">'
        . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset',
                [
                    'class' => 'btn btn-default'
                ]
        )
        . ' '
        . Html::submitButton('<i class="glyphicon glyphicon-floppy-save"></i> Submit',
                [
                    'class' => 'btn btn-primary', 'id' => 'btn-create-bp'
                ]
        )
        . '</div>';
        ActiveForm::end();
        ?>    
    </div>
</div>
