<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\search\BusCounterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'BUS COUNTERS';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-counter-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Add Counter', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '5px',
                ],
            ],
            [
                'attribute' => 'counterName',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '80px',
                ],
            ],
            [
                'attribute' => 'counterCode',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '80px',
                ],
            ],
            [
                'attribute' => 'address',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '80px',
                ],
            ],
            [
                'attribute' => 'phone',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '80px',
                ],
            ],
            [
                'attribute' => 'isActive',
                'format' => 'raw',
                'value' => function ($model) {
                    $activeStatus = $model->isActive;
                    if ($activeStatus == 0) {
                        $result = '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                    } else if ($activeStatus == 1) {
                        $result = '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                    }
                    return $result;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '90px',
                ],
            ],
            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'options' => [
                    'width' => 65
                ],
            ],
        ],
    ]);
    ?>


</div>
