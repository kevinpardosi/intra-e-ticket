<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BusCounter */

$this->title = 'BUS COUNTER - ADD NEW';
$this->params['breadcrumbs'][] = ['label' => 'Bus Counter', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-counter-create">
    
    <div class="bus-counter-header"><h2>FORM - ADD BUS COUNTER</h2></div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
