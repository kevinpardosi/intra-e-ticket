<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BusCounter */

$this->title = 'Update Bus Counter: ' . $model->busCounterId;
$this->params['breadcrumbs'][] = ['label' => 'Bus Counters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->busCounterId, 'url' => ['view', 'id' => $model->busCounterId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bus-counter-update">
    
    <div class="bus-counter-header"><h2>FORM - EDIT BUS COUNTER</h2></div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
