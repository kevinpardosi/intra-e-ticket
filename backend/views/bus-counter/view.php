<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\BusCounter */

$this->title = strtoupper($model->counterName);
$this->params['breadcrumbs'][] = ['label' => 'Bus Counters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bus-counter-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->busCounterId], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->busCounterId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'condensed' => true,
        'hover' => true,
        'enableEditMode' => false,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => 'CODE # ' . $model->counterCode,
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            [
                'attribute' => 'isActive',
                'format' => 'raw',
                'displayOnly' => true,
                'value' => function($form, $widget) {
                    $model = $widget->model;
                    $isActive = $model->isActive;
                    if ($isActive == 0) {
                        $result = '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                    } else if ($isActive == 1) {
                        $result = '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                    }
                    return $result;
                },
            ],
            'counterName',
            'counterCode',
            'address',
            'phone',
            [
                'attribute' => 'created_by',
                'displayOnly' => true,
                'value' => function($form, $widget) {
                    $model = $widget->model;
                    $modelUser = User::find()->where(['id' => $model->created_by])->one();
                    $creator = $modelUser->username;
                    return ucfirst($creator);
                },
            ],
            [
                'attribute' => 'created_at',
                'displayOnly' => true,
                'format' => ['date', 'php:d - m - Y']
            ],
            [
                'attribute' => 'updated_by',
                'displayOnly' => true,
                'value' => function($form, $widget) {
                    $model = $widget->model;
                    if ($model->updated_by != null) {
                        $modelUser = User::find()->where(['id' => $model->updated_by])->one();
                        $creator = $modelUser->username;
                        return ucfirst($creator);
                    }
                },
            ],
            [
                'attribute' => 'updated_at',
                'displayOnly' => true,
                'format' => ['date', 'php:d - m - Y']
            ],
        ],
    ]);
    ?>

</div>
