<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\builder\Form;

$form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);

/* @var $this yii\web\View */
/* @var $model backend\models\BusCounter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bus-counter-form">   

    <div class="col-sm-12">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'compactGrid' => true,
            'attributes' => [
                'counterName' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Nama Loket...']],
                'counterCode' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'ID Loket...']],                
            ]
        ]);
        ?>
    </div>

    <div class="col-sm-12">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'compactGrid' => true,
            'attributes' => [
                'address' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Masukkan Alamat Loket...']],
                'phone' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Nomor Telepon Loket...']],
                'isActive' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\select2\Select2',
                    'options' => [
                        'data' => [1 => "Active", 2 => "Inactive"],
                    ],
                ]
            ]
        ]);
        
        echo '<div class="text-right">'
        . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset',
                [
                    'class' => 'btn btn-primary'
                ]
        )
        . ' '
        . Html::submitButton('<i class="glyphicon glyphicon-floppy-save"></i> Submit',
                [
                    'class' => 'btn btn-primary', 'id' => 'btn-create-bp'
                ]
        )
        . '</div>';
        ActiveForm::end();
        ?>
    </div>
</div>
