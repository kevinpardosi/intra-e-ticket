<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Areas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="areas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'areaName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'areaDeptCount')->textInput() ?>

    <?= $form->field($model, 'areaDestCount')->textInput() ?>

    <?= $form->field($model, 'areaNote')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
