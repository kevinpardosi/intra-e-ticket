<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use backend\models\BusFleet;

/* @var $this yii\web\View */
/* @var $searchModel app\search\BusRoutesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ROUTES';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-routes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Add Route', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '5px',
                ],
            ],
            [
                'attribute' => 'busClassId',
                'value' => function ($model) {
                    return $model->busClass->busClassName;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '180px',
                ],
            ],
            [
                'attribute' => 'busDepartureId',
                'value' => function ($model) {
                    return $model->departureName->areaName;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '280px',
                ],
            ],
            [
                'attribute' => 'busDestinationId',
                'value' => function ($model) {
                    return $model->destinationName->areaName;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '280px',
                ],
            ],
            [
                'attribute' => 'ticketPrice',
                'format' => ['decimal', 0],
                'value' => function ($model) {
                    return $model->ticketPrice;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '120px',
                ],
            ],
            [
                'attribute' => 'busRouteCount',
                'label' => 'Total Trip',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '120px',
                ],
            ],
            [
                'class' => '\kartik\grid\ActionColumn',
                'header' => 'Action',
                'width' => '116px',
                'headerOptions' => [
                    'style' => 'color:#BD2B2B; text-align:center'
                ],
                'contentOptions' => [
                    'vertical-align: middle;'
                ]
            ],
        ],
    ]);
    ?>


</div>
