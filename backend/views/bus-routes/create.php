<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BusRoutes */

$this->title = 'BUS ROUTE - ADD NEW';
$this->params['breadcrumbs'][] = ['label' => 'Bus Route', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-routes-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
