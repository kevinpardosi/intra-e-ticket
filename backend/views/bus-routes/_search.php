<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\BusRoutesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bus-routes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'busRouteId') ?>

    <?= $form->field($model, 'busClassId') ?>

    <?= $form->field($model, 'busDepartureId') ?>

    <?= $form->field($model, 'busDestinationId') ?>

    <?= $form->field($model, 'ticketPrice') ?>

    <?php // echo $form->field($model, 'busRouteCount') ?>

    <?php // echo $form->field($model, 'busRouteNote') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
