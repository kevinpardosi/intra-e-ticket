<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\builder\Form;
use kartik\select2\Select2;
use backend\models\BusDrivers;
use backend\models\Areas;
use backend\models\BusClass;

$form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);

/* @var $this yii\web\View */
/* @var $model app\models\BusRoutes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bus-routes-form">

    <div class="bus-fleet-header"><h2>FORM - ADD ROUTE</h2></div>   

    <div class="col-sm-6">
        <?php
        echo '<label class="control-label">Departure</label>';
        echo Select2::widget([
            'model' => $model,
            'attribute' => 'busDepartureId',
            'disabled' => true,
            'data' => ArrayHelper::map(Areas::find()->all(), 'areaId', 'areaName'),
            'options' => [
                'value' => 51,
                'placeholder' => 'Select Departure...',
            ],
            'pluginOptions' => [
                'allowClear' => false
            ],
        ]);
        ?>
    </div>

    <div class="col-sm-6">
        <?php
        echo '<label class="control-label">Destination</label>';
        echo Select2::widget([
            'model' => $model,
            'attribute' => 'busDestinationId',
            'data' => ArrayHelper::map(Areas::find()->all(), 'areaId', 'areaName'),
            'options' => [
                'placeholder' => 'Select Destination...',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-sm-12">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'compactGrid' => true,
            'attributes' => [
                'busClassId' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\select2\Select2',
                    'options' => [
                        'data' => ArrayHelper::map(BusClass::find()->all(), 'busClassId', 'busClassName'),
                        'value' => null,
                    ],
                    'hint' => 'Pilih Tipe Class Armada Bus'
                ],
                'ticketPrice' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => [
                        'placeholder' => 'Ticket Price, e.g: Rp. 50.000',
                        'id' => 'ticket-price',
                    ],
                    'label' => 'Price',
                ],                
            ]
        ]);
        ?>
    </div>
    
    <div class="col-sm-12">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'compactGrid' => true,
            'attributes' => [
                'busRouteNote' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Insert the route additional note, e.g: Rute Perjalanan ini melalui jalur lintas barat...']],
            ]
        ]);

        echo '<div class="text-right">'
        . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset',
                [
                    'class' => 'btn btn-default'
                ]
        )
        . ' '
        . Html::submitButton('<i class="glyphicon glyphicon-floppy-save"></i> Submit',
                [
                    'class' => 'btn btn-primary', 'id' => 'btn-create-bp'
                ]
        )
        . '</div>';
        ActiveForm::end();
        ?>    
    </div>    
</div>
