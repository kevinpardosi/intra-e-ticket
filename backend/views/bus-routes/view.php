<?php

use yii\helpers\Html;
use backend\assets\DashboardAsset;
use kartik\detail\DetailView;
use app\models\User;
use backend\models\BusClass;
use backend\models\Areas;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\BusRoutes */

$dept = Areas::find()->where(['areaId' => $model->busDepartureId])->one();
$dest = Areas::find()->where(['areaId' => $model->busDestinationId])->one();

$this->title = $dept->areaName . ' - ' . $dest->areaName;
//$this->params['breadcrumbs'][] = ['label' => 'Bus Routes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
DashboardAsset::register($this);

//get bus class
$modelBusClass = BusClass::find()->where(['busClassId' => $model->busClassId])->one();
$busClassName = ucfirst($modelBusClass->busClassName);
$busClassCode = $modelBusClass->busClassCode;
?>
<div class="bus-routes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    echo DetailView::widget([
        'model' => $model,
        'id' => $model->busRouteId,
        'condensed' => true,
        'hover' => true,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => 'CLASS CODE # ' . $busClassCode,
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            [
                'attribute' => 'busClassId',                
                'label' => 'Bus Class',
                'value' => $busClassName,
                'format' => 'raw',                
                'type' => DetailView::INPUT_SELECT2,
                'widgetOptions' => [
                    'data' => ArrayHelper::map(BusClass::find()->all(), 'busClassId', 'busClassName'),
                ],
            ],
            [
                'attribute' => 'busDepartureId',
                'value' => $dept->areaName,
                'displayOnly' => true,
                'format' => 'raw',
                'type' => DetailView::INPUT_SELECT2,
                'widgetOptions' => [
                    'data' => ArrayHelper::map(Areas::find()->all(), 'areaId', 'areaName'),
                ],
            ],
            [
                'attribute' => 'busDestinationId',
                'value' => $dest->areaName,
                'format' => 'raw',                
                'type' => DetailView::INPUT_SELECT2,
                'widgetOptions' => [
                    'data' => ArrayHelper::map(Areas::find()->all(), 'areaId', 'areaName'),
                ],
            ],
            [
                'attribute' => 'ticketPrice',
                'value' => $model->ticketPrice,
                'format' => ['decimal', 0]
            ],
            [
                'attribute' => 'busRouteCount',
                'displayOnly' => true,
            ],
            [
                'attribute' => 'busRouteNote',
                'displayOnly' => true,
            ],                        
            [
                'attribute' => 'created_by',
                'displayOnly' => true,
                'value' => function($form, $widget) {
                    $model = $widget->model;
                    $modelUser = User::find()->where(['id' => $model->created_by])->one();
                    $creator = $modelUser->username;
                    return ucfirst($creator);
                },
                'updateMarkup' => function($form, $widget) {
                    $model = $widget->model;
                    $modelUser = User::find()->where(['id' => $model->created_by])->one();
                    $creator = $modelUser->username;
                    return ucfirst($creator);
                },
            ],
            [
                'attribute' => 'created_at',
                'displayOnly' => true,
                'format' => ['date', 'php:d - m - Y']
            ],
            [
                'attribute' => 'updated_by',
                'displayOnly' => true,
                'value' => function($form, $widget) {
                    $model = $widget->model;
                    if ($model->updated_by != null) {
                        $modelUser = User::find()->where(['id' => $model->updated_by])->one();
                        $creator = $modelUser->username;
                        return ucfirst($creator);
                    }
                },
                'updateMarkup' => function($form, $widget) {
                    $model = $widget->model;
                    if ($model->updated_by != null) {
                        $modelUser = User::find()->where(['id' => $model->updated_by])->one();
                        $creator = $modelUser->username;
                        return ucfirst($creator);
                    }
                },
            ],
            [
                'attribute' => 'updated_at',
                'displayOnly' => true,
                'format' => ['date', 'php:d - m - Y']
            ],
        ],
    ])
    ?>

</div>
