<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BusClass */

$this->title = 'Update Bus Class: ' . $model->busClassId;
$this->params['breadcrumbs'][] = ['label' => 'Bus Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->busClassId, 'url' => ['view', 'id' => $model->busClassId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bus-class-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
