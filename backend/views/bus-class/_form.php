<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BusClass */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bus-class-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'busClassCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'busClassName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'busClassNote')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
