<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BusClass */

$this->title = 'Create Bus Class';
$this->params['breadcrumbs'][] = ['label' => 'Bus Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-class-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
