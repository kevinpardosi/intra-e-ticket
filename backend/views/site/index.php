<?php

/* @var $this yii\web\View */

$this->title = 'INTRA E-TICKET';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>INTRA E-TICKET</h1>

        <p class="lead">Selamat datang di Sistem Informasi dan Transaksi CV. INTRA</p>

        <p><a class="btn btn-lg btn-primary" href="/intra/admin/trips-schedule/ticket-sales"><i class="fa fa-ticket"></i> Jual Tiket</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2></h2>

                <p><a class="btn btn-lg btn-success btn-block" href="http://www.yiiframework.com"><i class="fa fa-calendar-check-o"></i> BO Tiket</a></p>                
            </div>
            <div class="col-lg-4">
                <h2></h2>
                <p><a class="btn btn-lg btn-success btn-block" href="http://www.yiiframework.com"><i class="fa fa-book"></i> Data BO</a></p>
            </div>
            <div class="col-lg-4">
                <h2></h2>
                <div class="dropdown-report">
                <a class="btn btn-lg btn-success btn-block" href="http://www.yiiframework.com"><i class="fa fa-files-o"></i> Laporan</a>
                    <div class="dropdown-content">
                        <a href="#"><i class="fa fa-calendar-o"></i> Laporan Harian</a>
                        <a href="#"><i class="fa fa-calendar"></i> Laporan Bulanan</a>
                        <a href="#"><i class="fa fa-bar-chart"></i> Laporan BO Tiket</a>
                        <a href="#"><i class="fa fa-line-chart"></i> Laporan Keuangan</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<style>
/* Dropdown Button */
.btn-success {  
  font-size: 16px;
  padding:10px 16px;
  font-size:18px;
  line-height:1.3333333;
  border-radius:6px;
}

/* The container <div> - needed to position the dropdown content */
.dropdown-report {
  display:block;
  width:100%;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  width:91.5%;
  position: absolute;
  background-color: #f1f1f1;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown-report:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown-report:hover .dropbtn {background-color: #3e8e41;} 
</style>
