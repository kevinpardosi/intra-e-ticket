<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use backend\models\BusClass;
use backend\models\SystemStatus;

/* @var $this yii\web\View */
/* @var $searchModel app\search\BusFleetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'BUS FLEET';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-fleet-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Add Bus Fleet', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn',
                'header' => 'No',
            ],
            [
                'attribute' => 'busClassId',
                'value' => function ($model) {
                    $modelClass = BusClass::find()->where(['busClassId' => $model->busClassId])->one();
                    $result = $modelClass->busClassName;
                    return $result;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'busLicensePlate',
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],            
            [
                'attribute' => 'busSeatsCount',
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],            
            [
                'attribute' => 'busFleetStatus',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'value' => function ($model) {
                    $modelClass = SystemStatus::find()->where(['systemStatusId' => $model->busFleetId])->one();
                    $result = $modelClass->statusDetail;
                    return $result;
                },
            ],            
            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'options' => [                    
                    'width' => 65
                ],
            ],
        ],
    ]);
    ?>


</div>
