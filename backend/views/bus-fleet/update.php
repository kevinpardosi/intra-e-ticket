<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BusFleet */

$this->title = 'Update Bus Fleet: ' . $model->busFleetId;
$this->params['breadcrumbs'][] = ['label' => 'Bus Fleets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->busFleetId, 'url' => ['view', 'id' => $model->busFleetId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bus-fleet-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
