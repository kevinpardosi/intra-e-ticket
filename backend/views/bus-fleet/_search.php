<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\BusFleetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bus-fleet-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'busFleetId') ?>

    <?= $form->field($model, 'busClassId') ?>

    <?= $form->field($model, 'busLicensePlate') ?>

    <?= $form->field($model, 'busDriverId') ?>

    <?= $form->field($model, 'busCoDriverId') ?>

    <?php // echo $form->field($model, 'busSeatsCount') ?>

    <?php // echo $form->field($model, 'busFleetStatus') ?>

    <?php // echo $form->field($model, 'busFleetNote') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
