<?php

use yii\helpers\Html;
use backend\assets\DashboardAsset;
use kartik\detail\DetailView;
use app\models\User;
use backend\models\BusClass;
use backend\models\SystemStatus;
use kartik\editable\Editable;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\BusFleet */

$this->title = 'Bus - ' . $model->busLicensePlate;
//$this->params['breadcrumbs'][] = ['label' => 'Bus Fleets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->busLicensePlate;
DashboardAsset::register($this);

//get bus class
$modelBusClass = BusClass::find()->where(['busClassId' => $model->busClassId])->one();
$busClassName = ucfirst($modelBusClass->busClassName);
$busClassCode = $modelBusClass->busClassCode;
?>
<div class="bus-fleet-view">

    <h1><?= Html::encode($model->busLicensePlate) ?></h1>
    
    <?php
    echo DetailView::widget([
        'model' => $model,
        'id' => $model->busFleetId,        
        'condensed' => true,
        'hover' => true,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => 'CLASS CODE # ' . $busClassCode,
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            [
                'attribute' => 'busClassId',
                'label' => 'Bus Class',
                'value' => $busClassName,
            ],
            'busSeatsCount',
            'busFleetNote',
            [
                'attribute' => 'busFleetStatus',
                'format' => 'raw',
                'value' => Editable::widget([
                    'model' => $model,
                    'attribute' => 'busFleetStatus',
                    'beforeInput' => function($form, $widget) {
                        echo $form->field($widget->model, 'busFleetStatus')->widget(\kartik\select2\Select2::classname(), [
                            'data' => ArrayHelper::map(SystemStatus::find()->where(['statusCode' => 'busFleetStatus'])->all(), 'systemStatusId', 'statusDetail'),
                        ])->label(false);
                    },
                    'afterInput' => function($form, $widget) {
                        echo $form->field($widget->model, 'busFleetStatus')->widget(\kartik\select2\Select2::classname(), [
                            'data' => ArrayHelper::map(SystemStatus::find()->where(['statusDetail' => 'busFleetStatus'])->all(), 'systemStatusId', 'statusDetail'),
                        ])->label(false);
                    },
                    'size' => 'lg',
                    'header' => 'Bus Fleet Status',
                    'displayValueConfig' => [
                        1 => 'Operating',
                        2 => 'Maintenance',
                        3 => 'Available',
                        4 => 'Booked',
                    ],
                    'placement' => 'right',                            
                    'format' => Editable::FORMAT_LINK,
                    'inputType' => Editable::INPUT_SELECT2,
//                    'showAjaxErrors' => false,
//                    'formOptions' => [
//                        'action' => ['view', 'id' => $model->busFleetId],
////                        'method' => 'post',
////                        'enableAjaxValidation'=>true,
//                    ],
                    'pluginOptions' => [
//                        'ajax' => [
//                            'url' => \yii\helpers\Url::to(['bus-fleet/view']),
//                            'dataType' => 'json',
//                            'data' => new JsExpression("function (params) { return {q: params.term, p: params.page}; }"),
//                            'delay' => 250,
//                            'cache' => true,
//                        ],
                    ],
                ]),
            ],
            [
                'attribute' => 'created_by',
                'displayOnly' => true,
                'value' => function($form, $widget) {
                    $model = $widget->model;
                    $modelUser = User::find()->where(['id' => $model->created_by])->one();
                    $creator = $modelUser->username;
                    return ucfirst($creator);
                },
                'updateMarkup' => function($form, $widget) {
                    $model = $widget->model;
                    $modelUser = User::find()->where(['id' => $model->created_by])->one();
                    $creator = $modelUser->username;
                    return ucfirst($creator);
                },
            ],
            [
                'attribute' => 'created_at',
                'displayOnly' => true,
                'format' => ['date', 'php:d - m - Y']
            ],
            [
                'attribute' => 'updated_by',
                'displayOnly' => true,
                'value' => function($form, $widget) {
                    $model = $widget->model;
                    if($model->updated_by != null){
                        $modelUser = User::find()->where(['id' => $model->updated_by])->one();
                        $creator = $modelUser->username;
                        return ucfirst($creator);
                    }                    
                },
                'updateMarkup' => function($form, $widget) {
                    $model = $widget->model;
                    if($model->updated_by != null){
                        $modelUser = User::find()->where(['id' => $model->updated_by])->one();
                        $creator = $modelUser->username;
                        return ucfirst($creator);
                    }                    
                },
            ],
            [
                'attribute' => 'updated_at',
                'displayOnly' => true,
                'format' => ['date', 'php:d - m - Y']
            ],
        ],
    ])
    ?>   
</div>

<?php
$url = Url::toRoute(['bus-fleet/view?id=' . $model->busFleetId]);
$key = $model->busFleetId;
$this->registerJs(<<< JS
$(document).ready(function () {
    $('.kv-editable-submit').click(function() {               
        
        var key = '$key';
        var parAtt = $(".popover-title").clone()
            .children()
            .remove()
            .end()
            .text();               
        
        var preRep = parAtt.replace(/^\s+Edit /,"");
        
        var postRepF = preRep.replace(/\s+$/, "");
        
        var postRepS = postRepF.replace(/\s+/, "");        
        
        var upAtt = postRepS.replace(/\s+/, "");        
        
        var value = $("#select2-busfleet-busfleetstatus-container").text();
        
        var att = upAtt[0].toLowerCase() + upAtt.substring(1)
        
        $.ajax({     
            url: '$url',
            dataType: 'json',
            data:{hasEditable: 1, editableIndex: 0, editableKey: key, BusFleet: [{name: value, editableAttribute: att}]},
            type: 'post',
        });     
    });
});
JS
);
?>