<?php

use yii\helpers\Html;
use backend\assets\DashboardAsset;
use kartik\detail\DetailView;
use app\models\User;
use backend\models\BusClass;
use backend\models\SystemStatus;
use kartik\editable\Editable;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\BusFleet */

$this->title = 'Bus - ' . $model->busLicensePlate;
//$this->params['breadcrumbs'][] = ['label' => 'Bus Fleets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->busLicensePlate;
DashboardAsset::register($this);

//get bus class
$modelBusClass = BusClass::find()->where(['busClassId' => $model->busClassId])->one();
$busClassName = ucfirst($modelBusClass->busClassName);
$busClassCode = $modelBusClass->busClassCode;
?>
<div class="bus-fleet-view">

    <h1><?= Html::encode($model->busLicensePlate) ?></h1>
      <?php
                    echo Editable::widget([
                        'model' => $model,
                    'attribute' => 'busFleetStatus',
                    'beforeInput' => function($form, $widget) {
                        echo $form->field($widget->model, 'busFleetStatus')->widget(\kartik\select2\Select2::classname(), [
                            'data' => ArrayHelper::map(SystemStatus::find()->where(['statusCode' => 'busFleetStatus'])->all(), 'systemStatusId', 'statusDetail'),
                        ])->label(false);
                    },
                    'afterInput' => function($form, $widget) {
                        echo $form->field($widget->model, 'busFleetStatus')->widget(\kartik\select2\Select2::classname(), [
                            'data' => ArrayHelper::map(SystemStatus::find()->where(['statusDetail' => 'busFleetStatus'])->all(), 'systemStatusId', 'statusDetail'),
                        ])->label(false);
                    },
                    'size' => 'lg',
                    'header' => 'Bus Fleet Status',
                    'displayValueConfig' => [
                        1 => 'Operating',
                        2 => 'Maintenance',
                        3 => 'Available',
                        4 => 'Booked',
                    ],
                    'placement' => 'right',                            
                    'format' => Editable::FORMAT_LINK,
                    'inputType' => Editable::INPUT_SELECT2,
                        'formOptions' => [
                            'action' => Url::to(['bus-fleet/view2', 'id' => $model->busFleetId])
                        ],
                    
                    ]);
                    ?>
</div>