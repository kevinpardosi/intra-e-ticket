<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\BusDrivers;
use backend\models\BusClass;
use backend\models\SystemStatus;
use kartik\select2\Select2;
use kartik\builder\Form;

$form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);

/* @var $this yii\web\View */
/* @var $model app\models\BusFleet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bus-fleet-form">

    <div class="bus-fleet-header"><h2>FORM - ADD BUS FLEET</h2></div>

    <div class="col-sm-4">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'compactGrid' => true,
            'attributes' => [
                'busLicensePlate' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Tanda Nomor Kendaraan Bermotor...']],
            ]
        ]);
        ?>
    </div>
    <div class="col-sm-4">
        <?php
        echo '<label class="control-label">Driver</label>';
        echo Select2::widget([
            'model' => $model,
            'attribute' => 'busDriverId',
            'data' => ArrayHelper::map(BusDrivers::find()->all(), 'busDriverId', 'busDriverName'),
            'options' => [
                'placeholder' => 'Select Driver...',
            ],
            'pluginOptions' => [
                'allowClear' => false
            ],
        ]);
        ?>
    </div>
    <div class="col-sm-4">
        <?php
        echo '<label class="control-label">Co - Driver</label>';
        echo Select2::widget([
            'model' => $model,
            'attribute' => 'busCoDriverId',
            'data' => ArrayHelper::map(BusDrivers::find()->all(), 'busDriverId', 'busDriverName'),
            'options' => [
                'placeholder' => 'Select Co - Driver...',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>
    <div class="col-sm-12">
        <?php
        $model->busFleetStatus = 3;
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'compactGrid' => true,
            'attributes' => [
                'busClassId' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\select2\Select2',
                    'options' => [
                        'data' => ArrayHelper::map(BusClass::find()->all(), 'busClassId', 'busClassName'),
                        'value' => null,
                    ],
                    'hint' => 'Pilih Tipe Class Armada Bus'
                ],
                'busSeatsCount' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Jumlah Bangku...']],
                'busFleetStatus' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => '\kartik\select2\Select2',                    
                    'options' => [
                        'data' => ArrayHelper::map(SystemStatus::find()->where(['statusCode' => 'busFleetStatus'])->all(), 'systemStatusId', 'statusDetail'),
//                        'disabled' => true,
                    ],
//                    'hint' => 'Pilih Status Keadaan Armada Bus'
                ],
            ]
        ]);
        ?>
    </div>
    <div class="col-sm-12">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'compactGrid' => true,
            'attributes' => [
                'busFleetNote' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Catatan tambahan terkait armada bus...']],
            ]
        ]);        

        echo '<div class="text-right">'
        . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset',
                [
                    'class' => 'btn btn-primary'
                ]
        )
        . ' '
        . Html::submitButton('<i class="glyphicon glyphicon-floppy-save"></i> Submit',
                [
                    'class' => 'btn btn-primary', 'id' => 'btn-create-bp'
                ]
        )
        . '</div>';
        ActiveForm::end();
        ?>
    </div>
</div>
