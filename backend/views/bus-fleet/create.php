<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BusFleet */

$this->title = 'BUS FLEET - ADD NEW';
$this->params['breadcrumbs'][] = ['label' => 'Bus Fleet', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-fleet-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
