<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BusDrivers */

$this->title = 'Update Bus Drivers: ' . $model->busDriverId;
$this->params['breadcrumbs'][] = ['label' => 'Bus Drivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->busDriverId, 'url' => ['view', 'id' => $model->busDriverId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bus-drivers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
