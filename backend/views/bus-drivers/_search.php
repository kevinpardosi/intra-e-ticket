<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\BusDriversSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bus-drivers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'busDriverId') ?>

    <?= $form->field($model, 'busDriverCode') ?>

    <?= $form->field($model, 'busDriverName') ?>

    <?= $form->field($model, 'busDriverPhone') ?>

    <?= $form->field($model, 'busDriverAddress') ?>

    <?php // echo $form->field($model, 'driverStatus') ?>

    <?php // echo $form->field($model, 'licenseExpiredAt') ?>

    <?php // echo $form->field($model, 'is_expired') ?>

    <?php // echo $form->field($model, 'idCard') ?>

    <?php // echo $form->field($model, 'drivingLicense') ?>

    <?php // echo $form->field($model, 'busDriverNote') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
