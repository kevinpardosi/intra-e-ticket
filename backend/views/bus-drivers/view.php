<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\BusDrivers */

$this->title = 'Driver - ' . $model->busDriverName;
//$this->params['breadcrumbs'][] = ['label' => 'Driver', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->busDriverName;
\yii\web\YiiAsset::register($this);
?>
<div class="bus-drivers-view">

    <h1><?= Html::encode($model->busDriverName) ?></h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'condensed' => true,
        'hover' => true,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => 'CODE # ' . $model->busDriverCode,
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'busDriverPhone',
            'busDriverAddress',
            [
                'attribute' => 'driverStatus',
                'label' => 'Driving Role',
                'value' => $model->driverStatus ? 'Driver' : 'Co - Driver',
                'type' => DetailView::INPUT_SELECT2,
                'widgetOptions' => [
                    'data' => [1 => 'Driver', 2 => 'Co - Driver']
                ],
            ],
            [
                'attribute' => 'busDriverNote',
                'label' => 'Note'
            ],
            'licenseExpiredAt',
            'is_expired',
            'idCard',
            'drivingLicense',
            [
                'attribute' => 'created_by',
                'displayOnly' => true,
                'value' => function($form, $widget) {
                    $model = $widget->model;
                    $modelUser = User::find()->where(['id' => $model->created_by])->one();
                    $creator = $modelUser->username;
                    return ucfirst($creator);
                },
                'updateMarkup' => function($form, $widget) {
                    $model = $widget->model;
                    $modelUser = User::find()->where(['id' => $model->created_by])->one();
                    $creator = $modelUser->username;
                    return ucfirst($creator);
                },
            ],
            [
                'attribute' => 'created_at',
                'displayOnly' => true,
                'format' => ['date', 'php:d - m - Y']
            ],
            [
                'attribute' => 'updated_by',
                'displayOnly' => true,
                'value' => function($form, $widget) {
                    $model = $widget->model;
                    if($model->updated_by != null){
                        $modelUser = User::find()->where(['id' => $model->updated_by])->one();
                        $creator = $modelUser->username;
                        return ucfirst($creator);
                    }                    
                },
                'updateMarkup' => function($form, $widget) {
                    $model = $widget->model;
                    if($model->updated_by != null){
                        $modelUser = User::find()->where(['id' => $model->updated_by])->one();
                        $creator = $modelUser->username;
                        return ucfirst($creator);
                    }                    
                },
            ],
            [
                'attribute' => 'updated_at',
                'displayOnly' => true,
                'format' => ['date', 'php:d - m - Y']
            ],
        ]
    ]);
    ?>
</div>
