<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BusDrivers */

$this->title = 'Driver - Add New';
$this->params['breadcrumbs'][] = ['label' => 'Driver', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-drivers-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
