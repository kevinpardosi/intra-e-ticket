<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;

$form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);

/* @var $this yii\web\View */
/* @var $model app\models\BusDrivers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bus-drivers-form">
    <div class="bus-fleet-header"><h2>FORM - ADD DRIVER</h2></div>
    <?php
    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'compactGrid' => true,
        'attributes' => [
            'busDriverCode' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Insert the driver code number, e.g: KP - 001']],
            'busDriverName' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Insert the drver name, e.g: Kevin Pardosi']],
        ]
    ]);

    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'compactGrid' => true,
        'attributes' => [
            'busDriverPhone' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Insert the driver phone number, e.g: 081212122121']],
            'driverStatus' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => '\kartik\select2\Select2',
                'options' => ['data' => [1 => 'Driver', 2 => 'Co - Driver']],
                'hint' => 'Choose Driving Role'
            ],
        ]
    ]);

    echo Form::widget([// 1 column layout
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'compactGrid' => true,
        'attributes' => [
            'busDriverAddress' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Insert the driver address, e.g: Jl. Seksama, Medan']],
            'busDriverNote' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Insert the driver additional note, e.g: Susah berkendara dimalam hari karena mata mulai rabun']],
        ]
    ]);

    echo '<div class="text-right">'
    . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset',
            [
                'class' => 'btn btn-default'
            ]
    )
    . ' '
    . Html::submitButton('<i class="glyphicon glyphicon-floppy-save"></i> Submit',
            [
                'class' => 'btn btn-primary', 'id' => 'btn-create-bp'
            ]
    )
    . '</div>';
    ActiveForm::end();
    ?>

</div>