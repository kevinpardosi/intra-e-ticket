<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\BusDriversSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'DRIVERS';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-drivers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Add Driver', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn',
                'header' => 'No',
            ],
            [
                'attribute' => 'busDriverName',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => 250
                ],
            ],
            [
                'attribute' => 'busDriverPhone',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => 130
                ],
            ],
            [
                'attribute' => 'busDriverAddress',
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'driverStatus',
                'value' => function ($model) {
                    if ($model->driverStatus == 1) {
                        return "Driver";
                    } else {
                        return "Co - Driver";
                    }
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => 100
                ],
            ],
            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'options' => [
                    'width' => 65
                ],
            ],
        //'licenseExpiredAt',
        //'is_expired',
        //'idCard',
        //'drivingLicense',
        ],
    ]);
    ?>


</div>
