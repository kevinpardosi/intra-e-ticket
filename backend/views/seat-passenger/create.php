<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SeatPassenger */

$this->title = 'Create Seat Passenger';
$this->params['breadcrumbs'][] = ['label' => 'Seat Passengers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seat-passenger-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
