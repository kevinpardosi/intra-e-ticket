<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\search\SeatPassengerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Seat Passengers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seat-passenger-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Seat Passenger', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'seatPassengerId',
            'tripScheduleId',
            'seat-1',
            'seat-2',
            'seat-3',
            //'seat-4',
            //'seat-5',
            //'seat-6',
            //'seat-7',
            //'seat-8',
            //'seat-9',
            //'seat-10',
            //'seat-11',
            //'seat-12',
            //'seat-13',
            //'seat-14',
            //'seat-15',
            //'seat-16',
            //'seat-17',
            //'seat-18',
            //'seat-19',
            //'seat-20',
            //'seat-21',
            //'seat-22',
            //'seat-23',
            //'seat-24',
            //'seat-25',
            //'seat-26',
            //'seat-27',
            //'seat-28',
            //'seat-29',
            //'seat-30',
            //'seat-31',
            //'seat-32',
            //'seat-33',
            //'seat-34',
            //'seat-35',
            //'seat-36',
            //'seat-37',
            //'seat-38',
            //'seat-39',
            //'seat-40',
            //'seat-41',
            //'seat-42',
            //'isOcuppied',
            //'maintenanceSeat-1',
            //'maintenanceSeat-2',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
