<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\search\SeatPassengerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seat-passenger-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'seatPassengerId') ?>

    <?= $form->field($model, 'tripScheduleId') ?>

    <?= $form->field($model, 'seat-1') ?>

    <?= $form->field($model, 'seat-2') ?>

    <?= $form->field($model, 'seat-3') ?>

    <?php // echo $form->field($model, 'seat-4') ?>

    <?php // echo $form->field($model, 'seat-5') ?>

    <?php // echo $form->field($model, 'seat-6') ?>

    <?php // echo $form->field($model, 'seat-7') ?>

    <?php // echo $form->field($model, 'seat-8') ?>

    <?php // echo $form->field($model, 'seat-9') ?>

    <?php // echo $form->field($model, 'seat-10') ?>

    <?php // echo $form->field($model, 'seat-11') ?>

    <?php // echo $form->field($model, 'seat-12') ?>

    <?php // echo $form->field($model, 'seat-13') ?>

    <?php // echo $form->field($model, 'seat-14') ?>

    <?php // echo $form->field($model, 'seat-15') ?>

    <?php // echo $form->field($model, 'seat-16') ?>

    <?php // echo $form->field($model, 'seat-17') ?>

    <?php // echo $form->field($model, 'seat-18') ?>

    <?php // echo $form->field($model, 'seat-19') ?>

    <?php // echo $form->field($model, 'seat-20') ?>

    <?php // echo $form->field($model, 'seat-21') ?>

    <?php // echo $form->field($model, 'seat-22') ?>

    <?php // echo $form->field($model, 'seat-23') ?>

    <?php // echo $form->field($model, 'seat-24') ?>

    <?php // echo $form->field($model, 'seat-25') ?>

    <?php // echo $form->field($model, 'seat-26') ?>

    <?php // echo $form->field($model, 'seat-27') ?>

    <?php // echo $form->field($model, 'seat-28') ?>

    <?php // echo $form->field($model, 'seat-29') ?>

    <?php // echo $form->field($model, 'seat-30') ?>

    <?php // echo $form->field($model, 'seat-31') ?>

    <?php // echo $form->field($model, 'seat-32') ?>

    <?php // echo $form->field($model, 'seat-33') ?>

    <?php // echo $form->field($model, 'seat-34') ?>

    <?php // echo $form->field($model, 'seat-35') ?>

    <?php // echo $form->field($model, 'seat-36') ?>

    <?php // echo $form->field($model, 'seat-37') ?>

    <?php // echo $form->field($model, 'seat-38') ?>

    <?php // echo $form->field($model, 'seat-39') ?>

    <?php // echo $form->field($model, 'seat-40') ?>

    <?php // echo $form->field($model, 'seat-41') ?>

    <?php // echo $form->field($model, 'seat-42') ?>

    <?php // echo $form->field($model, 'isOcuppied') ?>

    <?php // echo $form->field($model, 'maintenanceSeat-1') ?>

    <?php // echo $form->field($model, 'maintenanceSeat-2') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
