<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SeatPassenger */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seat-passenger-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tripScheduleId')->textInput() ?>

    <?= $form->field($model, 'seat-1')->textInput() ?>

    <?= $form->field($model, 'seat-2')->textInput() ?>

    <?= $form->field($model, 'seat-3')->textInput() ?>

    <?= $form->field($model, 'seat-4')->textInput() ?>

    <?= $form->field($model, 'seat-5')->textInput() ?>

    <?= $form->field($model, 'seat-6')->textInput() ?>

    <?= $form->field($model, 'seat-7')->textInput() ?>

    <?= $form->field($model, 'seat-8')->textInput() ?>

    <?= $form->field($model, 'seat-9')->textInput() ?>

    <?= $form->field($model, 'seat-10')->textInput() ?>

    <?= $form->field($model, 'seat-11')->textInput() ?>

    <?= $form->field($model, 'seat-12')->textInput() ?>

    <?= $form->field($model, 'seat-13')->textInput() ?>

    <?= $form->field($model, 'seat-14')->textInput() ?>

    <?= $form->field($model, 'seat-15')->textInput() ?>

    <?= $form->field($model, 'seat-16')->textInput() ?>

    <?= $form->field($model, 'seat-17')->textInput() ?>

    <?= $form->field($model, 'seat-18')->textInput() ?>

    <?= $form->field($model, 'seat-19')->textInput() ?>

    <?= $form->field($model, 'seat-20')->textInput() ?>

    <?= $form->field($model, 'seat-21')->textInput() ?>

    <?= $form->field($model, 'seat-22')->textInput() ?>

    <?= $form->field($model, 'seat-23')->textInput() ?>

    <?= $form->field($model, 'seat-24')->textInput() ?>

    <?= $form->field($model, 'seat-25')->textInput() ?>

    <?= $form->field($model, 'seat-26')->textInput() ?>

    <?= $form->field($model, 'seat-27')->textInput() ?>

    <?= $form->field($model, 'seat-28')->textInput() ?>

    <?= $form->field($model, 'seat-29')->textInput() ?>

    <?= $form->field($model, 'seat-30')->textInput() ?>

    <?= $form->field($model, 'seat-31')->textInput() ?>

    <?= $form->field($model, 'seat-32')->textInput() ?>

    <?= $form->field($model, 'seat-33')->textInput() ?>

    <?= $form->field($model, 'seat-34')->textInput() ?>

    <?= $form->field($model, 'seat-35')->textInput() ?>

    <?= $form->field($model, 'seat-36')->textInput() ?>

    <?= $form->field($model, 'seat-37')->textInput() ?>

    <?= $form->field($model, 'seat-38')->textInput() ?>

    <?= $form->field($model, 'seat-39')->textInput() ?>

    <?= $form->field($model, 'seat-40')->textInput() ?>

    <?= $form->field($model, 'seat-41')->textInput() ?>

    <?= $form->field($model, 'seat-42')->textInput() ?>

    <?= $form->field($model, 'isOcuppied')->textInput() ?>

    <?= $form->field($model, 'maintenanceSeat-1')->textInput() ?>

    <?= $form->field($model, 'maintenanceSeat-2')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
