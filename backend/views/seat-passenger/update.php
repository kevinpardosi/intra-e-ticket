<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SeatPassenger */

$this->title = 'Update Seat Passenger: ' . $model->seatPassengerId;
$this->params['breadcrumbs'][] = ['label' => 'Seat Passengers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->seatPassengerId, 'url' => ['view', 'id' => $model->seatPassengerId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="seat-passenger-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
