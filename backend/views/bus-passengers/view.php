<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use backend\models\TripsSchedule;
use backend\models\BusRoutes;

/* @var $this yii\web\View */
/* @var $model app\models\BusPassengers */

$this->title = 'TRIP TICKET';
$this->params['breadcrumbs'][] = ['label' => 'Bus Passengers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bus-passengers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php    
//        $seatsArray = explode(",", $model->seatNumber);
//        $seats = json_encode($seatsArray);
    ?>

<!--    <div>        
        <button type="button" onclick="setOccupied()" class="btn btn-set" id="set-occupied-id"><?php // echo $seats;?></button>
    </div>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->busPassengerId], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->busPassengerId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?php
    $attributes = [
        [
            'group' => true,
            'label' => 'PASSENGER',
            'groupOptions' => ['style' => 'color: #31708F', 'class' => 'text-center']#abdde5;
        ],
        [
            'columns' => [
                [
                    'attribute' => 'busPassengerName',
                    'label' => 'Name',
                    'displayOnly' => true,
                    'valueColOptions' => ['style' => 'width:30%']
                ],
                [
                    'attribute' => 'busPassengerAddress',
                    'label' => 'Address',
                    'valueColOptions' => ['style' => 'width:30%'],
                    'displayOnly' => true
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'busPassengerPhone',
                    'label' => 'Phone',
                    'valueColOptions' => ['style' => 'width:30%'],
                    'displayOnly' => true
                ],
                [
                    'attribute' => 'seatNumber',
                    'label' => 'Total Seat ( ' . '<kbd>' . 'Seat Number' . '</kbd>' . ' )',
                    'format' => 'raw',
                    'value' => '<b>' . $model->totalSeats . '</b>' . ' ( ' . '<kbd>' . $model->seatNumber . '</kbd>' . ' )',
                    'valueColOptions' => ['style' => 'width:30%'],
                    'displayOnly' => true
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'isMultiSeat',
                    'format' => 'raw',
                    'label' => 'Multi Seat',
                    'valueColOptions' => ['style' => 'width:30%'],
                    'value' => function($form, $widget) {
                        $model = $widget->model;
                        if ($model->isMultiSeat == 0) {
                            $isMultiSeat = '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                        } else {
                            $isMultiSeat = '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                        }
                        return $isMultiSeat;
                    },
                    'displayOnly' => true
                ],
                [
                    'attribute' => 'isLuggage',
                    'format' => 'raw',
                    'label' => 'Lugagge',
                    'valueColOptions' => ['style' => 'width:30%'],
                    'value' => function($form, $widget) {
                        $model = $widget->model;
                        if ($model->isLuggage == 0) {
                            $isLuggage = '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                        } else {
                            $isLuggage = '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                        }
                        return $isLuggage;
                    },
                    'displayOnly' => true
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'unitPrice',
                    'label' => 'Unit Price (Rp)',
                    'valueColOptions' => ['style' => 'width:30%'],
                    'format' => ['decimal', 0],
                    'displayOnly' => true
                ],
                [
                    'attribute' => 'totalPrice',
                    'label' => 'Total (Rp)',
                    'valueColOptions' => ['style' => 'width:30%'],
                    'format' => ['decimal', 0],
                    'displayOnly' => true
                ],
            ],
        ],
        [
            'rowOptions' => ['style' => 'border-top: 10px double #dedede; background-color:#ddd'],
            'columns' => [
                [
                    'attribute' => 'totalPaid',
                    'label' => 'Cash (Rp)',
                    'format' => ['decimal', 0],
                    'displayOnly' => true
                ],
                [
                    'attribute' => 'restPayment',
                    'label' => 'Rest Payment (Rp)',
//                            'valueColOptions' => ['style' => 'width:30%'],
                    'format' => ['decimal', 0],
                    'displayOnly' => true
                ],
                [
                    'attribute' => 'totalChange',
                    'label' => 'Change (Rp)',
//                            'valueColOptions' => ['style' => 'width:30%'],
                    'format' => ['decimal', 0],
                    'displayOnly' => true
                ],
            ],
        ],
        [
            'group' => true,
            'label' => 'BUS',
            'rowOptions' => ['class' => 'table-info'],
            'groupOptions' => ['style' => 'color: #31708F', 'class' => 'text-center']#abdde5;
        ],
        [
            'columns' => [
                [
                    'attribute' => 'tripScheduleId',
                    'label' => 'Route',
                    'value' => function($form, $widget) {
                        $modelTrip = new TripsSchedule();
                        $model = $widget->model;
                        $route = $model->trip->busRouteId;
                        return $model::getRouteName($route);
                    },
                ],
                [
                    'attribute' => 'tripScheduleId',
                    'label' => 'Departure At',
                    'value' => function($form, $widget) {
                        $model = $widget->model;
                        $trip = $model->tripScheduleId;
                        return $model::getDepartureAt($trip);
                    },
                ],
            ]
        ],
            ]
    ?>

    <?php
    echo DetailView::widget([
        'model' => $model,
        'id' => $model->busPassengerId,
        'condensed' => true,
        'enableEditMode' => false,
        'hover' => true,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => '<i class="fa fa-ticket"></i> ' . strtoupper('TICKET DETAILS') . ' (' . $model->bookingCode . ')',
            'type' => DetailView::TYPE_INFO,
//            'headerOptions' => [
//                'back'
//            ]
        ],
        'attributes' => $attributes,
    ])
    ?>

</div>
