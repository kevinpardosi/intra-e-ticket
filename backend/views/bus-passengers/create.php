<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BusPassengers */

$this->params['breadcrumbs'][] = ['label' => 'Bus Passengers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bus-passengers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'tripId' => $tripId,
        'seatNumber' => $seatNumber,
    ]) ?>

</div>
