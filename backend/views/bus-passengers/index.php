<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\BusPassengersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bus Passengers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-passengers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bus Passengers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'busPassengerId',
            'tripScheduleId',
            'busPassengerName',
            'busPassengerAddress',
            'busPassengerPhone',
            //'unitPrice',
            //'totalSeats',
            //'totalPrice',
            //'totalPaid',
            //'restPayment',
            //'isMultiSeat',
            //'bookingCode',
            //'seatNumber',
            //'isLuggage',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
