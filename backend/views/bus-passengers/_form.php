<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use backend\controllers\BaseController;
use kartik\select2\Select2;
use yii\i18n\Formatter;

$seatOptionsValue = true;
$form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);

$base = new BaseController();
$data = $base->selectSeat($tripId);
$unitPrice = $base->getUnitPrice($tripId);

/* @var $this yii\web\View */
/* @var $model app\models\BusPassengers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bus-passengers-form">
    <div class="bus-passenger-header"><h2>BUY TICKET - PASSENGER</h2></div>

    <div class="col-sm-3">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'compactGrid' => true,
            'attributes' => [
                'isMultiSeat' => [
                    'type' => Form::INPUT_CHECKBOX,
                    'options' => [
                        'id' => 'is-multi-seat-id'
                    ]
                ],
                'isLuggage' => [
                    'type' => Form::INPUT_CHECKBOX,
                    'options' => [
                        'id' => 'is-luggage-id'
                    ]
                ],
            ]
        ]);
        ?>
    </div>  

    <div class="col-sm-5">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'compactGrid' => true,
            'attributes' => [
                'bookingCode' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => [
                        'placeholder' => 'Masukkan Kode Booking Penumpang...',
                        'id' => 'booking-code-id',
                    ]
                ],
            ]
        ]);
        ?>
    </div>  

    <div class="col-sm-4">
        <?php
        echo '<label class="control-label">Seat Number</label>';
        echo Select2::widget([
            'model' => $model,
            'attribute' => 'seatNumber',
            'data' => $data,
//            'disabled' => true,
            'options' => [
                'value' => $seatNumber,
                'placeholder' => 'Select Seat...',
                'multiple' => true,
                'id' => 'seat-number-options-id'
            ],
            'pluginOptions' => [
                'allowClear' => false
            ],
        ]);
        ?>
    </div>


    <div class="col-sm-12">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'compactGrid' => true,
            'attributes' => [
                'busPassengerName' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Nama Penumpang...']],
                'busPassengerPhone' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Nomor Handphone Penumpang...']],
                'busPassengerAddress' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Masukkan Alamat Penumpang...']]
            ]
        ]);
        ?>
    </div>   

    <div class="col-sm-12">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 6,
            'compactGrid' => true,
            'attributes' => [
                'unitPrice' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => [
                        'value' => Yii::$app->formatter->asDecimal($unitPrice, NULL, [\NumberFormatter::MAX_FRACTION_DIGITS => 0]),
                        'readOnly' => 'true',
                        'id' => 'unit-price-id',
                    ]
                ],
                'totalSeats' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => [
                        'value' => '1',
                        'readOnly' => 'true',
                        'id' => 'total-seat-id'
                    ]
                ],
                'totalPrice' => [
                    'type' => Form::INPUT_TEXT,
                    'format' => 'raw',
                    'options' => [
                        'readOnly' => 'true',
                        'id' => 'total-price-id'
                    ]
                ],
                'totalPaid' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => [
                        'placeholder' => 'Total Pembayaran ...',
                        'id' => 'total-paid-id'
                    ]
                ],
                'restPayment' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => [
                        'placeHolder' => '0',
                        'readOnly' => 'true',
                        'id' => 'rest-payment-id'
                    ]
                ],
                'totalChange' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => [
                        'placeHolder' => '0',
                        'readOnly' => 'true',
                        'id' => 'total-change-id'
                    ]
                ],
            ]
        ]);
        ?>
    </div>   

    <div class="col-sm-12">
        <?php
        echo '<div class="text-right">'
        . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset',
                [
                    'class' => 'btn btn-primary'
                ]
        )
        . ' '
        . Html::submitButton('<i class="glyphicon glyphicon-floppy-save"></i> Submit',
                [
                    'class' => 'btn btn-primary', 'id' => 'btn-create-ticket-passenger-id'
                ]
        )
        . '</div>';
        ActiveForm::end();
        ?>
    </div>

</div>
