<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [        
        'css/bootstrap.min.css',
        'css/style/css/font-awesome.min.css',
//        'http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        'css/ionicons.min.css',
        'css/AdminLTE.min.css',
        'css/skins/_all-skins.min.css',
        'plugins/iCheck/flat/blue.css',
        'css/site.css',
        'css/morris.css',
        'css/e-ticket.css',
        'css/jquery-jvectormap.css',
        'css/bootstrap-datepicker.min.css',
        'css/daterangepicker.css',
        'css/google-font-family.css',
        'plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css',
//        'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
    ];
    public $js = [        
        'js/eticket-intra.js',
        'js/bootstrap.bundle.min.js',
//        'js/bootstrap.min.js',
        'js/jquery-ui.min.js',
//        'https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
        'plugins/raphael/raphael.min.js',
        'plugins/sparkline/jquery.sparkline.min.js',
        'plugins/slimScroll/jqeury.slimscroll.min.js',
        'plugins/fastclick/fastclick.min.js',        
        'js/app.min.js',
        'js/dashboard.js',             
        'js/Popper.js',         
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    
    public $img = [
        'images/seat.png',
    ];
}
